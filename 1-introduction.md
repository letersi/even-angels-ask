
# Introduction

## "Brothers, I Lost Him" {-}

The most recollected image from William Somerset Maugham's novel _Of
Human Bondage_ has to be the Persian rug,[^2] the one that Philip, the
main character, repeatedly contemplates, following the advice of a
friend, in an effort to discover the meaning of life. Upon reaching an
especially low point in a young existence filled with terrible and
senseless tragedies, lived and observed, he wanders the streets of
London. And, once again, the oriental rug possesses his mind. As
Philip contemplates its intricate designs, he is suddenly seized by
a wondrous insight: the carpet does indeed contain the answer to the
riddle that has vexed him. He finally sees that life, like the
interwoven patterns of the rug, is always complex, sometimes beautiful
and sometimes bewildering -- but, in the end, utterly without meaning or
purpose.

[^2]: William S. Mangham, _Of Human Bondage_ (New York: Vintage Books. 1961).

It was difficult for many critics to empathize with Philip's
subsequent contentment at his discovery, and this, I believe,
contributed to the book's mostly ambivalent initial reviews. However,
those who reached the same conclusion as Philip after their own
conservative religious upbringing could surely understand.

_Of Human Bondage_ is a very western novel, part of a genre of
literary works that explored the same difficult issues and often
arrived at similar conclusions. Not that other cultures entirely
bypassed conflicts of faith and reason, but western civilization seems
to have been at the front the longest, battling such problems for well
over two thousand years. While the declarations of victory for science
and rationalism some twenty-five years ago were probably premature, it
does appear that religion has had the worse of it and may lose even
more of what little ground it still possesses. The ways we live our
lives are shaped by the meanings we read into them, and the course of
this battle has greatly informed our society's viewpoint. The question
of the purpose of life is fundamental, and we can hardly know a person
or a society until we understand how this question is treated.

It is common these days to hear psychologist discuss the modern crisis
of meaning. C. G. Jung, one of the first to recognize and publicize
it, remarked that most of his patients over the age of forty were
suffering from it in one form or another.[^3] Where the answers
supplied by religion once satisfied a largely illiterate western
Europe, in modern times religious dogmas are only deepening the crisis
and alienating many from spiritual considerations. Some reject
religion entirely, and many who preserve some ties to a faith may find
it in conflict with their rational thinking. The result is that
religion is pushed ever farther towards the back of the shelf and
substitutes must be found for the answers and services that belief
once provided. Jung, and many others after him, claimed that this
trend will continue unless faith can be made to conform with current
knowledge and experience. This is seldom seen as a positive trend,
because it appears that human nature includes spirituality and that
this can not be ignored in what seems to be our instinctive need to
see our lives as meaningful. Victor Frankl frequently states that if
one can provide man with a positive "why" to live, he or she will
inevitably find a positive and productive "how" to live. But it cannot
be just any "why"; it must be one that he or she finds compelling
rationally, intellectually, and spiritually.[^4]

[^3]: C. G. Jung, _An Answer to Job_, trans. by R. F. C. Hull (New York: Meridian Books, 1960).

[^4]: Victor Frankl, _Man's Search for Meaning_, trans. by I. Lasch (Boston: Beacon Press, 1992).

Enter the Muslim. The last three decades have witnessed a sudden
growth in the American Muslim community, spurred mostly by immigration
and African American conversion since the civil rights era. The Muslim
also finds himself being drawn into the same conflict. With full
confidence he critiques, "I don't think that the two major religions
in America make much sense."

He receives the reply, "I don't think that any religion makes much
sense. For example, from your religion's viewpoint, what is the
purpose of life? Why did God create us to suffer here on earth?"

The Muslim thinks back on what he was taught as a child. "I believe He
created us to test us."

Of course he is then asked, "So your religion rejects the omniscience
of God, for otherwise, what could He possibly learn from testing us
that He does not already know?"

The Muslim feels cornered and searches his past for the universally
accepted answers he was forced to memorize. "No, that is not quite it. Ah!
Yes!! We are created to worship Him!"

With a sly smile his opponent inquires, "Then you must believe that
God has needs and weaknesses, for why else would He demand our
worship? When a human demands our devotion, we label him a tyrant or
psychopath. Do you hold that God has character flaws?"

The Muslim's head is now spinning in ill-defined questions and doubts.
He gropes for a clue from his childhood. It comes to him! "Adam sinned
and his punishment was this earthly life!"

His adversary has the cool, calculated look of one about to say
"Checkmate! Putting aside scientific difficulties, it appears that you
believe that God is unjust; for why punish all of Adam's descendants
for Adam's sin?  Why not give each his own chance? Do you Muslims also
believe in original sin?"

"No! No! Of course not!"

The game is over.

A Muslim living in America may have to confront these questions many
times; for they are part of the intellectual basis of western
civilization. Sometimes the result may be a loss, even an abandonment,
of faith.  Salmon Rushdie is, for many Muslims, the archetypal
case. He confessed in an interview that he had been a very religious
child. However, during his education in England, his faith was shaken
severely by western attitudes toward religion and Islam in
particular. His case is not unique. I have met many university
professors in America with Muslim names who disavow any belief in
Islam and quite a few who go out of their way to make a point of it.

Most often, however, the faith of a Muslim immigrant will remain
intact. He may feel a bit rattled or he might retreat slightly from
his belief that "Islam makes sense" to the position that "Islam makes
more sense", but the fervor of his commitment, for the most part, will
abide, because it is grounded in the life-long experience of being a
Muslim. Born into an environment where Islam is practically
universally accepted -- where it is to one's disadvantage to be a
non-Muslim -- his faith has had the opportunity to take root and grow
unimpeded. Through many years of steady participation and practice
there came security, pride, meaning, community, and perhaps, awe
inspiring, spiritual encounters -- maybe even perceived miracles -- that
together made the sweetness of faith more real and powerful than any
challenge some logical sleight of hand could provide.

The convert's state is more precarious, and there has been a somewhat
high rate of apostasy among them in recent years. Whether a convert will
remain a Muslim for long usually depends on what originally brought him
to the decision and whether that initial need continued to be met long
enough to root him in the religion. Frequently, if the initial motivating fac­
tor fades and the convert finds that certain negative aspects of being a
Muslim outweigh the perceived benefits, the option to leave the community
is taken. *Like the immigrant, the convert's commitment to the religion will
depend on his or her personal, emotional, and spiritual faith experiences;
the same could be said of any religion in the West.* As the convert is fully
part of the surrounding society and subjected to its intellectual challenges
and criticisms from the very start of his entry into Islam, questions of faith
and reason may have greater influence on his religious choices than they do
for immigrant Muslims.

Yet the future of Islam in the West, and America in particular, is not
primarily about immigrants and converts; it is about children, and the
"success" of Islam in Europe and America will be measured by the
religiosity of their descendants. The grandchildren of today's Muslims
in the West will undoubtedly be western in their attitudes and
thinking -- their survival depends on it -- but to what degree they will
claim allegiance to Islam or the worldwide Muslim community is far
from certain.

At the 1993 ISNA meeting in Tulsa, Oklahoma, Dr. Jamal Badawi recalled
how, during a recent visit to Australia, he saw a good number of
buildings that resembled the mosques that one sees in Muslim lands.
However, they were being put to secular uses, such as office space,
meeting halls, and the like. He was informed that there was once a
large population of Afghani immigrants to Australia and that the
now-converted mosques are the last remnants of a community that had
been completely assimilated by the dominant culture. Dr. Badawi used
this example to highlight the urgent need for Islamic schools in
America.

Islamic schools may help Muslim children preserve their religious
identity, but there looms the larger issues of what and how they are
taught.  Many an agnostic or atheist today attended church or
synagogue schools as children. If a religious community is to produce
leading scholars and scientists, its approach towards education will
have to be compatible with modem critical analytical methods of study;
this, I feel, is absolutely necessary. It requires an environment of
free inquiry and expression, where self-criticism and objectivity are
encouraged and questioning and doubt tolerated. If the approach to
general education conflicts with that of religious education, then the
students will be left with a choice, perhaps a perpetual one, between
alternative modes of thinking. In this way, religion for its children
will be delimited -- westernized, so to speak; it will become a
compartment of thought to be entered into in limited situations and
abandoned in others.

For Muslim children, this dilemma is particularly acute. If Islam can
not be shown to be in harmony with rational thought, then faith for
the western Muslim, like many adherents to other religions in the
West, becomes solely a personal, experiential, and spiritual
matter. It loses much of its persuasiveness. Reasoning can be
communicated quite effectively, but not spiritual experiences. We
cannot really share our mystical encounters; we can only interpret and
approximate them. I am not saying that faith can exist on an
exclusively rational level or that it cannot exist in disharmony with
reason. What I am saying is that if a rationally compelling case is
not made for Islam, one that Muslim young people in the West can
relate to, then Islam will be seen by many of them as just another
religion, a religious option among more or less equal options. In
addition, in an environment where their religion is greatly feared,
where of all the great world religions theirs is the most despised,
where its rituals and practices are the most demanding, where its
constraints seem to go against the larger society's trends and
lifestyles­ -- in such an environment, we should not at all be surprised
if a significant fraction of children born to Muslim parents leave
aside the faith they inherited.

A few years ago, I read an article in a Muslim American magazine that
stated, according to a study it had conducted, that nine out of ten
children born to Muslim parents in America either become atheists or
claim allegiance to no particular religion by the time they reach
college age. The article did not state what statistical methods were
used to obtain this fraction, so I doubted its reliability. But even
if only half as many of these children ultimately leave Islam, it will
still be a crisis for Muslims in America. Yet perhaps such a statistic
should not be such a shock. Why should American children born of
Muslim parents be very different from those born of Buddhist or Hindu
parents or of any religion unfamiliar to the West, especially in
consideration of some of the above-mentioned special obstacles they
face?

It is said that it takes about three generations before an immigrant
family becomes fully assimilated into American society. I have not
conducted a scientific study, but I have met through my teaching at
the university a good number of third-generation Americans of Muslim
descent, and so far, I have not found a single one who professes
belief in Islam. When I ask these students if they are Muslims, the
typical response I obtain is "My parents are", which is exactly the
same answer I used to give when asked if I was a Christian. This may
paint an excessively pessimistic picture, since the grandparents of
these young people were part of an infinitesimally small Muslim
minority group. Now that there are suddenly several million Muslims
in America and Canada, we should expect that a large number of their
grandchildren will identify themselves as such, although it remains to
be seen to what extent this will reflect an active religious
commitment. In my travels to various Islamic conferences in America, I
always ask about the participation of young people in the local Muslim
communities; I inevitably find that it is extremely small.

I first became interested in the relevance of Islam to American Muslim
children a little over ten years ago when I lived in San
Francisco. One night, after the evening ritual prayer, about a dozen
of us sat in a circle on the floor of the _masjid_ engaged in
chitchatting. We had been led in the prayer that night by Muhammad,
who, at forty, was the oldest among us and one of the most loved and
respected members of our community. Someone asked him how his son was
doing, as we had not seen him in some time. He answered that he had
turned sixteen that day, and the little room was immediately filled
with smiles, laughter, and congratulations, for our oldest boy had
become a man. However, Muhammad was not sharing in our joy, and we
suddenly all fell silent, because we saw large, round teardrops
falling from Muhammad's downcast face. He looked up and his voice
cracked as he exclaimed, "Brothers, I lost him -- I lost my son!"

No explanation was necessary. We had seen or heard of too many similar
cases in the neighboring communities. If his son, at sixteen, was
still attached to the religion, he would have been an exception; it
was only that our confidence and esteem for Muhammad was so great. All
we could do was sit there, speechless, belittled by an irresistible
and unfeeling power.

As I drove home from the _masjid_ that night, I could not rid myself of
the expression on Muhammad's face or the desperation in his plea. I
thought about my first child, who was soon to be born, and how I would
be feeling sixteen years from now. The more I thought about the whole
matter, the less I found myself in agreement with Muhammad. I was not
convinced that he had lost his son, because I was not sure that he had
really found him.

Muhammad was a truly devout Algerian Muslim who had done everything
to raise a good Algerian Muslim son. But his son was not an Algerian;
he was as American as apple pie, and whatever used to work back in
Algeria had failed in America, as it did for so many others.

We used to remark how very quiet Muhammad's boy was at our community
functions. A child's silence may be a sign of respect or assent, but
it could also represent indifference to what is being said. I wondered
if Muslim American children had as much difficulty relating to the
perspectives and traditions of the mosque as I did.

Through the years, conversations with Muslim children and parents
supported this suspicion and led me to conjecture that if the
Americanness that I shared with Muslim young people was alienating
many of us from the viewpoint of the mosque, then perhaps at some
stage in their lives these children may come to relate to what other
Americans and I discovered in Islam. As I discussed the matter with
other converts, it emerged that our reflections intersected at many
key points that approximate a certain characteristic path to
Islam. Therefore, I would like to take you, the reader, along that
path. I would like to invite you to a journey: _A Journey to Islam in
America_.

You need to know what to pack and how we will get to our destination.
To the first of these the answer is: as little as possible. You are
requested to leave behind as much religious baggage as you can;
ideally, you should pretend you are an atheist, with perhaps many
objections to belief in God, yet open-minded enough not to dismiss a
point of view without at least considering it. As for the second
question, our guide will be the Qur'an, the principle source of
guidance and spiritual compass of billions of Muslims, and, for many
newcomers to Islam, their main introduction to the faith.

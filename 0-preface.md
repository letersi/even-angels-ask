
# Preface {-}

Summer has become my favorite season, with its sunshine, warmth, long
days and long walks in the late afternoon; but when I was twelve, it
was definitely winter, with its blizzards and snowball fights,
ice-football, sled riding and ice hockey! It was also the time of year
in which I could steal a few hours alone with my father every once and
a while, because the rest of the year he had to work twelve hours a
day, seven days a week, at his one-man refrigeration and
air-conditioning business. On certain Sundays we would take our German
shepherd for a walk along the beach.  My father always picked the most
bitter days: stormy ones, bleak and gray and frigid, that remind me
now of the prize-winning photographs and paintings he had made when he
was younger. On one of these walks, we had almost reached the point
where we would usually turn back, when I looked up at him and asked,
"Dad, do you believe in heaven?"

I knew that I could depend on a straight answer with no punches
pulled.  My mom would carefully estimate the possible effects of her
answers to such questions before she finally framed one, while my
father's response would be the same irrespective of the
questioner. But he would not answer thoughtlessly, nor for that matter
dispassionately; he would personalize and internalize the issue.

He showed no reaction and kept on walking. We continued on into the
cold, damp, pounding gales that were blowing back his thick, graying
hair and stinging my face. I began to wonder if he had heard me, for
it must have been a half mile ago when I had asked him, then he slowed
to a stop and turned towards the shoreline. He was looking out past
Long Island when he said, almost to himself, "I could believe in Hell
easily enough, because there's plenty of that here on earth, but
heaven" -- he paused for a few seconds and then shook his head -- "I can't
conceive of it."

I was not entirely surprised, even at that young an age. My father was
extremely sensitive and already I understood that life had robbed him
of his goals and dreams. Every night after work he would try to numb
his anger, but more often than not it would lead to violent eruptions
of his frustrated passions. Anguish is contagious, and his angry, dark
and cynical view of life infected all of us -- after all, we were only
children and we were emotionally defenseless. When others of my
generation felt cheated by the Kennedy assassinations or Vietnam or
Watergate, I was hardly moved; they were only confirmations of what I
had already learned.

Religious types will criticize my father for opening a door, but it
had been unlocked for some time or I would not have asked the
question. If anything my father's answer slowed my inevitable
progress towards atheism, for he was not an irreligious man. The fact
that he had doubts seemed entirely natural -- how can any sane and
rational mind not have them? -- but he was nonetheless a believer and he
must have had some reason to believe.  Whatever it was, I never found
out.

I continued to have problems with heaven, because every time I
imagined that it was within God's power to create such a world, I had
to wonder why He created this one. Why, in other words, did He not
place us permanently in heaven from the start, with us free of the
weaknesses for which He would punish us with earthly suffering? Why
not simply make us into angels or something better? Of course, I heard
all the talk of God's infinite sense of justice; but _I_ did not choose
my nature; _I_ did not create temptation; _I_ did not ask to be born; and
_I_ did not eat from the tree! Did it occur to no one that the
punishment far exceeded the crime?! Even if only an allegory, it does
tell us something of the divine nature, something that is extremely
difficult to reconcile with "love" and "mercy".

How I came to despise these words! They made me sick with revulsion.
Not only were we here for no good reason, but an infinitely innocent
sacrifice and acceptance of a blatant contradiction were required
before admitting us into paradise. The rest of us, not lucky enough to
be born into the right creed or unable to suspend our reason, were
destined to be consigned to eternal damnation. Would it not, for
goodness sake, have been better to simply leave us as a bad idea never
realized?

It was all the sugar-coating that made belief for me so unpalatable. I
used to conceal my disregard behind an emotionless exterior as I
listened to assurances of divine love; like when you halfheartedly
humor someone you feel has lost his mind. When it was clear that I was
hopeless, we would invariably revert to the real issue: The Divine
Threat! "But what if you are wrong?" I was told; as if you should
believe on a hunch, in order to hedge your bets in case this brutal,
monstrous vision is a reality.

"Then if I am wrong, I will still be right: for refusing to surrender
to the irrational demands of an infinite tyranny, for refusing to
indulge an unquenchable narcissism that feeds on helpless suffering,
and for refusing to accept responsibility and repent for a grand
blunder which I did not commit. In the end, I will be an eternal
victim of the greatest injustice and in this way, I will forever
represent a higher sense of righteousness than the One that brought us
into being. It may not ease my suffering in the everlasting torture
chamber, but at least it will give it meaning."

## "So how in God's name did you become a Muslim?" {-}

This book is not meant to be an explanation, but the reader should be
able to piece together a rational one, perhaps something of an
emotional, psychological and spiritual one as well. Frankly, I am not
entirely sure exactly how it happened. So much of it seems to have
been outside my control, manipulated according to certain key
decisions I made along the way.  For the curious, they should know
that to become a Muslim requires simultaneous commitment to three
interrelated principles: the first: _there is no god_; the second: _but
God_; the third: _Muhammad is the messenger of God_.  What I have written
so far outlines how I came to the first of these. [^1]

[^1]: I explain -- or perhaps I should say interpret -- my conversion to
Islam in greater detail in my first book: Jeffrey Lang, _Struggling
to Surrender_ (Beltsville, MD: **amana publications**, 1994).

This book, however, has other motivations. It, like my first one, is
written first and foremost to my children, with the hope that my
struggle may somehow help them in their search for meaning. I want
them to understand where I am coming from: that this subject was never
for me an academic curiosity -- an exercise in rational thought; that I
have much more than an interest in it; that it is part of my past,
present, and future, part of my seeking, suffering, and desire. The
question I asked my father burned inside me, as does what I have
learned from it, and I cannot but share that with them. Yet I would
hate to see that their search end with mine. My greatest hope is that
they begin where I leave off, because no human has a complete and
final answer. To rely on someone's past insights is the gravest error,
for our knowledge grows with time, and to dogmatize an opinion is to
stop our progress towards the truth and to make way for atrophy and
decay.

I take no credit for whatever is good in the pages that follow -- I did not
find it, but it finds me -- and for that which is not, I rely on the forgiving
kindness of one whose wisdom illuminates our deeds according to our
intentions.

**REMARK:** I am concerned that the nature of this book may lead the
non-Muslim reader to an erroneous and one-sided image of
Muslims. Perhaps the western media's continued demonization of Muslims
and their religion has caused me to be oversensitive about this
matter.

The reader should keep in mind that this book is written primarily for
Muslims by one who was once outside their community, who tried,
especially in chapters three, five and six, to discuss those behaviors
and conceptions of modern believers that he has had difficulty
understanding, adjusting to, or accepting. It therefore contains a
fair amount of criticism of Muslims. I also have shared with the
reader many of the setbacks I have suffered in my own struggle to
achieve surrender to God, and I fear that my example may be more
discouraging than most

Let me take this opportunity then to assert that in the past twenty
years I have met a multitude of virtuous, warm, noble, magnanimous,
deeply religious Muslims, from whom I have gained so much in knowledge
and friendship. If I wrote a book about the inspiring examples of
Muslims I have known, and someday I may, it would be of greater length
than this one. Similarly, if I wrote a critique of other communities
to which I belong -- for example, the American, the white Anglo-Saxon, or
the academic -- I am sure it would include far more numerous and more
severe criticisms than this book contains.

**REMARK:** It is a long established and cherished tradition among
Muslims to follow the mention of a prophet's name by the benediction
"may peace be upon him". In time, this practice was adopted in
writing, although the most ancient extant manuscripts show that this
custom was not adhered to rigidly by Muslim writers in the first two
Islamic centuries. To avoid interrupting the flow of ideas, I have not
followed the customary practice. I will simply take this occasion to
remind the Muslim reader of this tradition.

**REMARK:** The transliterations of Arabic words in this book are my
own approximations. Each introduction of a transliterated term in the
text is followed by an English translation in parenthesis. Experts
should be able to discern the corresponding Arabic terms and the
transliterations should pose no disadvantage to non-experts.

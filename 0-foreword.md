
# Foreword {-}

When, in 1996, I attended the annual convention of the Islamic Society
of North America in Columbus, Ohio, I ran into Dr. Jeffrey Lang's book
_Struggling to Surrender_ at the **amana** bookstand. At first I thought
that it was just another "confession" -- popular since St. Augustine and
Abu Hamid al Ghazali -- in which converts (or reconverts)
enthusiastically explain their very special way to their very special
religion.

How astonished I was when I realized that this book was of
major general import, very well written (as one might not expect from
a math teacher) and well researched. Yes, it _was_ a vivid description
of how Jeffrey Lang, almost torn apart in the process, felt
irresistibly drawn to Islam. But the book also offered a solid, well
reasoned platform for all other Americans who, like him, require
considerable depth of rational inquiry before surrendering to Allah's
call.

_Even Angels Ask_, Dr. Lang's second book -- not without significance,
written after a year's stay in Saudi Arabia -- shows the very same
virtues: Total honesty, common sense, a rigorous level of theological
investigation, and a thrilling oscillation between gifted story
telling and exposition of doctrine. Again, the author demonstrates
that (if only as a mathematician) he can only believe in a religion
that he finds compelling -- rationally, intellectually and
spiritually -- and that the religion is Islam: A thinking man's faith.

When the author alleges that (Christian) religious dogmas in modem
times are only deepening the crisis of faith and religion, he echos a
prediction made by Muhammad Asad (author of _Islam at the Crossroads_
and a leading Muslim intellect of the 20th century) in 1934 when he
foresaw that the doubts raised by the Nicene Creed, especially the
notions of incarnation and trinity, would not only alienate thinking
people from their churches but from the belief in God as
such. Dr. Lang is also in line with an observation made by Karen
Armstrong (author of _On God_) according to which Judaism suffered from
closing itself off (by considering itself as the "Chosen People")
while Christianity suffered from the opposite, its universality (by
absorbing a multitude of traditions into itself). Islam, according to
Dr. Lang, is positioned to avoid both pitfalls, and I agree with him.

In Saudi Arabia, the author came to realize that for him "there was no
escape from being an American," i.e., an "investigative Muslim", whose
way of inquiry into the basis of Islam was considered dangerous, even
suspected of leading to "innovation" and even heresy. (To be sure,
there is not a single instance in which the author's approach leads to
even the slightest deviation from the tenets of Islam.) The
conservative attitude that Dr. Lang encountered overseas, had years
earlier also affected Muhammad Asad (alias Leopold Weiss). From my
association with him I am certain that he would have endorsed both of
the author's books wholeheartedly.

Given this background, the book's title is not just an opening gambit
but a program: According to the Qur'an, _surat_ _al Baqarah_ _ayah_ 30, even
the angels (who are never rebellious) were moved to question God's
wisdom of creating man (who is rebellious and mischievous). Thus,
Muslims too must never stop asking pertinent questions about God, the
world and themselves.  Nevertheless, in view of the opposite orthodox
view, it takes some courage for Dr. Lang to propose that every
generation of Muslims is obliged to reinvestigate the foundations of
its faith "since knowledge grows with time".  As a matter of fact, he
holds that it would be a grave error, indeed, to rely blindly on past
judgments and to "dogmatize opinion", unless one is willing to
accept "atrophy and decay".

This, of course, the author certainly does not accept. On the
contrary, he tackles head-on many rather delicate issues, like
questions about predestination and theodicy. He offers no solutions
to these problems but points out, like Immanuel Kant (_Critique of Pure
Reason_) before him, that they cannot be solved because of man's
captivity in his mind's categories of time and space, proper to him
alone. Thus the author lifts unsoluble problems to a higher level of
awareness. More cannot be asked.

Even more important is, however, his substantiated critique of Muslim
shortcomings, both inside the United States and aboard. In particular,
he denounces

- subculture trends within the American Muslim community;
- the lack of tolerance between Muslim schools of thought;
- the dominance of Middle Eastern and Arabic features of merely
  cultural, not religious significance;
- traditional Muslim attitudes toward women that violate Qur'anic
  norms often causing Muslim women to feel unwelcome in mosques;
- over-focusing on nonessential, marginal aspects of the Islamic way
  of life, instead of looking for the general ethical and spiritual
  lessons of the Prophet's Sunnah; and
- the systematic distrust shown by "native" Muslims toward
  contributions by Western converts.

Jeffrey Lang wrote this book first and foremost for his
children -- leading them through the Qur'an in an eye-opening way and
introducing them to the five pillars of Islamic worship in a manner
which stresses spirituality rather than legalistic routine. In that,
he has done a tremendous service once more to all Muslim parents in
the United States who are often wondering, worrying and fretting
whether in a permissive, consumer and drug-oriented society it is
possible to transmit their faith to the next generation. In this
respect, the author seems to be somewhat pessimistic. I, however, am
inclined to see things in a more optimistic light -- if only for one
reason: There are two good books that just might move the scales in
favor of Islam -- _Struggling to Surrender_ and _Even Angels Ask_ by Jeffrey
Lang.

Murad Hofmann
Istanbul, April 1997

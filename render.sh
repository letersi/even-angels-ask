#!/bin/bash
### render gitbook

cd $(dirname $0)
rm -rf public/

Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook', output_dir = 'public')"
Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book', output_dir = 'public')"
Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::epub_book', output_dir = 'public')"

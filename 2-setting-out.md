# Setting Out

## Approaches to the Qur'an {-}

It would probably be better to delay this discussion for the
appendices, so as not to lose many readers before even getting
started, because I expect that the majority will be Muslims and that
many of them find this subject upsetting. It has to do with the role
of symbolism in revelation. The appendices may be more appropriate,
because the main conclusions reached in this chapter will hardly be
affected by these remarks, but my principle concern is with a smaller
readership, which, while attempting to reconcile religion with modern
thought, might be dissuaded by its own excessive literalism.

Muslims assert that the Qur'an is a revelation appropriate for all
persons, times, and places, and it is not difficult to summon Qur'anic
verses to support this claim. If they held to the opposite, there
would not be much point in considering their scripture. In order to
entertain this premise sincerely, we certainly should allow for, even
anticipate, that the Qur'an would use allegory, parables, and other
literary devices to reach a diverse audience. The language of the
Qur'an would have to be that of the Prophet's milieu and reflect the
intellectual, religious, social, and material customs of the
seventh-century Arabs. But if the essential message is universal, then
it must transcend the very language and culture that was the vehicle
of revelation. Since a community's language grows with and out of its
experiences, how then are realities outside that experience
communicated? There appears to be only one avenue: through the
employment of allegory, that is, the expression of truths through
symbolic figures and actions or, as the famous Qur'an exegete
Zamakhshari put it, "a parabolic illustration, by means of something
which we know from our experience, of something that is beyond the
reach of our perception." [^5]

[^5]: Muhammad Asad, _The Message of the Qur'an_ (Gibraltar: Dar al
Andalus, 1980), 989-91.

For example, the Qur'an informs us that Paradise in the hereafter is
such that "no person knows what delights of the eye are kept hidden
from them as a reward for their deeds"
([32:17](https://quran.com/32/17){target="_quran"}). Yet it also
provides very sensual images of Paradise that are particularly suited
to the imagination of Muhammad's contemporaries. These descriptions
recall the luxury and sensual delights of the most wealthy
seventh-century Bedouin chieftains. If the reader happened to be a man
from Alaska, he may be quite apathetic to these enticements. He may
prefer warm sandy beaches to cool oases; sunshine to constant shade;
scantily clad bathing beauties to houris, with the issue of whether or
not they are virgins of no real consequence. This reader will probably
take these references symbolically, reinforced by the Qur'an's
frequent assignment of the word _mathal_ (likeness, similitude,
example) to its eschatological descriptions.

Similarly, though God is "sublimely exalted above anything that men
may devise by way of definition"
([6:100](https://quran.com/6/100){target="_quran"}) and "there is
nothing like unto Him"
([42:11](https://quran.com/42/11){target="_quran"}) and "nothing can
be compared to Him"
([112:4](https://quran.com/112/4){target="_quran"}), the reader
nonetheless needs to relate to God and His activity. Thus we find that
the Qur'an provides many _comparative descriptions_ of God. For
instance, while human beings are sometimes merciful, compassionate,
generous, wise and forgiving, God is The Merciful, The Compassionate,
The Generous, The Wise, and The Forgiving. The Qur'an mentions God's
"face", "hand", "throne" and other expressions

> which at first sight have an almost anthropomorphic hue, for
instance, God's "wrath" (_ghadab_) or "condemnation"; His "pleasure"
at good deeds or "love" for His creatures; or His being "oblivious" of
a sinner who was oblivious of Him; or "asking" a wrongdoer on
Resurrection Day about his wrongdoing; and so forth," [^6]

[^6]: Ibid

To disallow the possibility of symbolism in such expressions would
seem to imply contradictions between some statements in the Qur'an. To
do so is entirely unnecessary, especially in consideration of the
following key assertion:

> He it is Who has bestowed upon you from on high this divine writ,
containing messages clear in and of themselves (_ayat muhkamat_) --
and these are the foundation of the divine writ -- as well as others
that are allegorical (_mutashabihat_). Now those whose hearts are
given too swerving from the truth go after that pan of it which has
been expressed in allegory, seeking out confusion, and seeking its
final meaning, but none save God knows its final
meaning. ([3:7](https://quran.com/3/7){target="_quran"})

Therefore the Qur'an itself insists on its use of symbolism, because
to describe the realm of realities beyond human perception -- what the
Qur'an designates as _al ghayb_ (the unseen or imperceptible) -- would
be impossible otherwise. This is why it would be a mistake to insist
on assigning a literal interpretation to the Qur'an's descriptions of
God's attributes, the Day of Judgment, Heaven and Hell, etc., because
the _ayat mutashabihat_ do not fully define and explicate these, but
they relate to us, due to the limitations of human thought and
language, something similar. This helps explain the well-known
doctrine of _bila kayf_ (without how) of al Ash'ari, the famous
tenth-century theologian whose viewpoint on this matter became
dominant in Muslim thought. It states that such verses reveal truths,
but we should not insist on, or ask, how these truths are
realized.[^7]

[^7]: Annemarie Schimmel, _Islam, An Introduction_ (New York: SUNY
Press, 1992), 78-81.

Throughout Muslim history, the literalist trend in Qur'an exegesis was
one among a number of approaches. Today, in America and Canada, it has
emerged as the most prevalent. It appears that the majority of Muslim
lecturers in America tend to take every narrative or description in
the Qur'an as a statement of a scientific or historical fact. So, for
example, the story of Adam is assumed to relate the historical and
scientific origins of Homo Sapiens. This tendency is reinforced by the
current widespread excitement over recent Qur'an and Science studies,
where many, if not most, of the discoveries of modern science are
believed to have been anticipated by the Qur'an.

It is true that some of the descriptions in the Qur'an of the "signs"
(ayat) in nature of God's wisdom and beneficence bear a fascinating
resemblance to certain modem discoveries, and it is also true that
none of these signs can be proved to be in conflict with science. But
part of the reason for this may argue against attempts by Muslims to
subject the Qur'an to scientific scrutiny.[^8] The Qur'an is very far
from being a science textbook. Its language is of the highest literary
quality and open to many different shades of meaning. The descriptions
of many of the Qur'an's signs that are believed today to predict
recently established facts appear to be consistently and intentionally
ambiguous, avoiding a degree of explicitness that would conflict with
any reader's level of knowledge of whatever era. If the Qur'an
contained a precise elaboration of these phenomena (the big bang
theory, the splitting of the atom, the expansion of the universe, to
name a few), these would have been known to ancient Muslim
scientists. A truly wondrous feature of the Qur'an is that these signs
lose nothing of their power, beauty, and mystery from one generation
to the next; each generation has found them compatible with the
current state of knowledge. To be inspired with awe and wonder by the
Qur'anic signs is one thing; to attempt to deduce or impose upon them
scientific theories is another and, moreover, is contrary to the
Qur'an's style.

[^8]: See for example, Malik Bennabi, _The Qur'anic Phenomenon_,
trans. by A.B. Kirkary (Plainfield, IN: American Trust Publications,
1983); Maurice Bucaille, _The Bible, the Qur'an and Science_ (Paris:
Seghers Publishers, 1977); and Keith L. Moore, _The Developing Human_
(appendix to 3d edition) (Philadelphia: W. B. Saunders Co., 1982). As
I remarked in _Struggling to Surrender_, this topic is interesting and
sometimes fascinating, but it too often requires complicated and
unobvious extrapolations in interpreting certain words and phrases.
This trend exists in other religious communities as well. A speaker
informed his audience that the New Testament contains the big bang
theory of creation, for in John it states that in the beginning was
the "word". Since a word is a single entity in the universe of
language that when voiced produces a vibration of sound, we obtain by
some isomorphism to the physical universe the theory of a single
original point mass of infinite density that explodes!

The relationship between the Qur'an and history is very much the same.
Anyone familiar with the Bible will notice that there are many
narratives in the Qur'an that have Biblical parallels. In the past,
Orientalists would accuse Muhammad, whom they assumed to be the
Qur'an's author, of plagiarizing or borrowing material from Jewish and
Christian sources. This opinion has become increasingly unpopular
among western scholars of Islam. For one thing, where Biblical
parallels do exist, the Qur'anic accounts almost always involve many
key differences in detail and meaning. Equally important is the fact
that the Qur'an itself assumes that its initial hearers were fairly
well acquainted with these tales. It is therefore very probable that
through centuries of contact, Jews, Christians, and pagans of the
Arabian peninsula adopted, with modifications, each other's oral
traditions. It also would not be at all surprising that traditions
shared by Jews and Arabs of the Middle East would go back to a common
source, since they shared a common ancestry. Hence, the conjecture
that the Qur'an borrows from the Bible is inappropriate.

In addition to biblical parallels, the Qur'an contains a number of
stories that were apparently known only to the Arabian peninsula and
at least one of mysterious origins.[^9] A striking difference between
all of the Qur'anic accounts and the biblical narratives is that while
the latter are very often presented in a historical setting, the
former defy all attempts to do so, unless outside sources are
consulted. In other words, relying exclusively on the Qur'an, it is
nearly impossible to place these stories in history. The episodes are
told in such a way that the meaning behind the story is emphasized
while extraneous details are omitted. Thus, western readers who know
nothing of the Arabian tribes of 'Ad and Thamud readily understand the
moral behind their tales. This omission of historical detail adds to
the transcendent and universal appeal of the narratives, for it helps
the reader focus on the timeless meaning of the stories.

[^9]: See the discussion on page 14 concerning Dhul Qarnain.

The Qur'anic stories are so utterly devoid of historical reference
points that it is not always clear whether a given account is meant to
be taken as history, a parable, or an allegory. Consider the following
two verses from the story of Adam:

> It is We Who created you, then We gave you shape, then We bade the
angels, "Bow down to Adam!" and they bowed down; not so Iblis; he
refused to be of those who bow
down. ([7:11](https://quran.com/7/11){target="_quran"})

> And when your Lord drew forth from the children of Adam, from their
loins, their seed, and made them testify of themselves, (saying) "Am I
not your Lord?" They said, "Yes, truly we testify." (That was) lest
you should say on the Day of Resurrection: "Lo! of this we were
unaware." ([7:172](https://quran.com/7/172){target="_quran"})

Note the transition in [7:11](https://quran.com/7/11){target="_quran"}
from "you" (plural in Arabic) in the first two clauses to "Adam" in
the third, as if mankind is being identified with Adam. These verses
seem to demand symbolic interpretations, otherwise from the first we
would have to conclude that _we_ were created, then _we_ were given
shape, then the command was given concerning the first man!  As for
[7:172](https://quran.com/7/172){target="_quran"}, I would not even
know how to begin to interpret this verse concretely, and it should
come as no surprise that many ancient commentators also understood it
symbolically.

The Qur'an's eighteenth _surah_, _al Kahf_, relates a number of
beautiful stories in an almost surrealistic style. For example, verse
86, from the tale of Dhul Qarnain reads,

> Until, when he reached the setting of the sun, and he found it
setting in a muddy spring, and found a people near it.  We said: "O
Dhul Qarnain! Either punish them or treat them with kindness."
([18:86](https://quran.com/18/86){target="_quran"})

This verse has puzzled Muslim commentators, many of whom searched
history for a great prophet conqueror that might compare to Dhul
Qarnain, who reached the lands where the sun rises and sets. The most
popular choice was Alexander the Great, which is patently false since
he is well known to have been a pagan. Since the sun does not
literally set in a muddy spring with people nearby, a
less-than-literal interpretation is forced upon us.

Rather than belabor the point, let me summarize my position. On the
basis of the style and character of the Qur'an, I believe that the
most general and most cautious statement one can make is: The Qur'an
relates many stories, versions with which the Arabs were apparently
somewhat familiar, not for the sake of relating history or satisfying
human curiosity, but to "draw a moral, illustrate a point, sharpen the
focus of attention, and to reinforce the basic message."[^10] I would
advise against attempts to force or decide the historicity of each of
these stories. First of all, because the Qur'an avoids historical
landmarks and since certain passages in some narratives clearly can
not be taken literally, such an insistence seems
unwarranted. Furthermore, imposing such limitations on the Qur'an may
lead, unnecessarily, to rational conflicts and obstructions that
distract the reader from the moral of a given tale. The Qur'an itself
harshly criticizes this inclination in _Surat al Kahf_:

[^10]: G. R. Hawting and Abdul-Kader A. Shareef, _Approaches to the
Qur'an_ (New York: Routledge, 1993), from the article by Ismael
K. Poonawala, "Darwaza's principles of modern exegesis," p. 231.

> Some say they were three, the dog being the fourth among
them. Others say they were five, the dog being the sixth -- doubtfully
guessing at the unknown. Yet others say seven, the dog being the
eighth. Say, "My Lord knows best their number, and none knows them,
but a few. Therefore, do not enter into controversies concerning them,
except on a matter that is clear."
([18:22](https://quran.com/18/22){target="_quran"})

Moreover, it would be humanly impossible to definitively decide the
historic or symbolic character of every tale; no one has the requisite
level of knowledge of history and Arab oral tradition -- not to
mention insight into the intent and wisdom of the author -- to make
such a claim. Personal ignorance should be admitted, but it should not
be allowed to place limits and bounds on the ways and means of
revelation.

As we set out on our journey, we will be approaching the Qur'an from
the standpoint of meaning; seeking to make sense of and find purpose
in the existence of God, man, and life. We are now ready to embark. We
have made our preparations and have broken camp. With the Qur'an
before us, we enter the first page.

## An Answer to a Prayer {-}

> In the Name of God, The Merciful, The Compassionate 1. Praise be to
> God, Lord of the worlds; 2. The Merciful, The Compassionate;
> 3. Master of the day of Requital; 4. You do we serve and You do we
> beseech for help; 5. Guide us on the straight path; 6. The path of
> those whom you have favored; 7. Not those upon whom is wrath and not
> those who are lost.
> ([1:1-7](https://quran.com/1/1-7){target="_quran"})

Volumes upon volumes have been written on the Qur'an's opening
_surah_, even though it consists of only seven short verses, but we,
my fellow travelers, have only time enough to pause for a few very
brief observations.

The first verse indicates a hymn of praise, to God, "the Lord of the
worlds". The divine names, "The Merciful, The Compassionate",
appearing in the second, head every _surah_ but one (the ninth) and
are among the most frequently mentioned attributes of God, both in the
Qur'an and by Muslims in their everyday speech. The mood abruptly
changes in verse two as it reawakens deep-seated anxieties and
conflicts. No sooner are God's mercy and compassion emphasized than we
are threatened with the "Day of Requital". Would it not have been more
tactful to postpone such considerations, to wait until the reader is a
little more comfortable with and confident in the Qur'an?  Assertions
about God's mercy, compassion, gentleness, or love never drove us from
religion, but warnings of a Day of Judgment, of Hell, of eternal
damnation that we found impossible to harmonize with mercy and
compassion, did.

The fourth verse goes even deeper into the quagmire as it reminds us
that service is rendered and pleas for help are directed to the very
creator of the predicament from which we seek salvation. Far from
allowing us to warm up to its message, the scripture wastes no time
recalling our complaints against religion. We will discover that this
is a persistent tactic of the Qur'an; that it repeatedly agitates the
skeptic by confronting him with his personal objections. We will soon
see that this Qur'an is no soft sell nor hard sell; that in reality it
is no sell at all; that it is no less than a challenge, a dare, to
fight and argue against this book.

We can relate to the last three verses all too readily. Life is a
chaotic puzzle, a random and confusing maze of paths and choices that
lead no where but to broken dreams, empty accomplishments, unfulfilled
expectations, one mirage after another. Is there a right path, or are
all in the end equally meaningless? Note the transition from personal
to impersonal in verses six and seven, as if to say that to obtain the
"straight path" is a divine favor conferred on those who seek and heed
divine guidance and that those who do not follow divine guidance are
exposed to all of life's impersonal, unfeeling wrath, and utter loss
and delusion. This wrath and loss we know well, for we have absorbed
life's anger and aimlessness and made it our own; it is our argument
for the nonexistence of a personal God and the foundation of our
philosophy.

We moved through the seven verses quickly. There was a subtle shift in
mood from the first four that glorified God to the last three that
asked for guidance. More than likely our first reading of them was so
casual that we did not observe the change. It was not until we had
finished the opening _surah_ that we realized that we had just
involuntarily and semiconsciously made a supplication. We were almost
tricked into it before we had a chance to resist. The beginning of the
next _surah_ will inform us that whether we consciously intended it or
not, our prayer has reached its destination and that it is about to
receive an answer.

## That Is the Book {-}

> In the name of God, the Merciful, The Compassionate 1. _Alif lam
> mim_ 2. That is the book, wherein no doubt, is guidance to those who
> have fear, 3. Who believe in the unseen, and are steadfast in prayer
> and spend out of what We have given them, 4. And who believe in that
> which is revealed to you and that which was revealed before you, and
> are certain of the hereafter.  5. These are on guidance from their
> Lord, and these, they are the successful.
> ([2:1-5](https://quran.com/2/1-5){target="_quran"}) [^11]

[^11]: The interpretations of verses from the Qur'an in this book are
for the most part my own, although I have relied heavily on a number
of well-known interpreters to guide me: Asad, _Message of the Qur'an_;
Muhammad Ali, _The Holy Qur'an, Text, English Translation and
Commentary_ (Lahore, Pakistan: Ahmadiyyah Anjuman Isha'at Islam,
1973); Abdullah Yusuf Ali, _The Meaning of the Holy Qur'an, New
Edition with Revised Translation, Commentary_ (Beltsville, MD: amana
publications, 1989); Marmaduke W. Pickthall, _The Meaning of the
Glorious Qur'an_ (New York: Muslim World League, 1977).

_Alif lam mim_ is a transliteration of the three Arabic letters that
open this _surah_. Twenty-nine _surahs_ begin with such letter
combinations of the Arabic alphabet. They continue to be a mystery to
Qur'an commentators, and opinions differ as to their meaning. Most
believe they are abbreviations of words or mystic symbols, but we will
leave such speculation aside.

The second verse declares to us that _that_ book, the Qur'an -- that
we have before us -- is without doubt the answer to the prayer we had
just recited. The tenor of the Qur'an from here on is different from
that of the opening _surah_. In the first _surah_, it was the reader
humbly petitioning God for guidance, while the perspective of the
remainder of the Qur'an, as this verse insists, will be God, in all
His supreme power and grandeur, proclaiming to the reader the guidance
that he sought, whether consciously or unconsciously, knowingly or
unknowingly. Also observe that doubt and fear are accentuated in this
verse. We should not exclude ourselves so hastily from these
attributes, even though we do not fit the full description continued
in verses three through five. We do have doubts, not only about God's
existence but also about our denial of it. If we were absolutely
certain of our atheism, we would not be reading this scripture.  As
much as we hate to admit it, we are not quite sure of ourselves; there
exists in us at least a glimmer of doubt -- and of fear. The word
_muttaqin_, translated as "those who have fear", comes from the Arabic
root which means to protect, to guard, to defend, to be cautious. It
implies an acute alertness to one's potential weaknesses, a person on
his toes, a self-critical awareness. We may not be believers, but we
are definitely guarded, defensive and cautious when it comes to
religion, otherwise we would not be atheists; we would have simply
accepted what we inherited from our parents. These are the same
qualities that brought us to this journey, because we suspect that we
might be wrong, that there is at least a chance that there is a God
and if there is, then we are ignoring what would have to be the most
important fact in our being.

Verse two also begins a description of the Qur'an's potential
audience. Like many a book of knowledge, it describes the
prerequisites and predisposition necessary to fully benefit from its
contents. The most sincere in their belief in God
([2:2-5](https://quran.com/2/2-5){target="_quran"}) will profit the
most. They believe in realities beyond their perceptions and are
devout and are kind to their fellow man. They have faith in what is
currently being revealed to them, which are the same essential truths
of all ages.

Verse six refers to the rejecters, who refuse to even consider the
Qur'an. The Qur'an in turn promptly dismisses them in the next verse.
Verse eight begins a relatively lengthy discussion
([2:8-20](https://quran.com/2/8-20){target="_quran"}) of all those in
between, who waver between belief and disbelief, often distracted and
blinded by worldly pursuits. From the standpoint of the Qur'an, we may
be towards the boundary of this category. Verses twenty through
twenty-nine outline some of the Qur'an's major themes: Man's need to
serve the one God, the prophethood of Muhammad, the hereafter and
final judgment, the Qur'an's use of symbolism
([2:26](https://quran.com/2/26){target="_quran"}), the resurrection of
man, and God's ultimate sovereignty.

Verse thirty begins the story of man. We will proceed slowly here,
line by line, since this has a strong bearing on our questions. The
ancient Qur'anic commentators would endorse such an approach, for they
used to speak of the _ijaz_ of the scripture -- its inimitable
eloquence that combines the most beautiful and yet most economical
expression. They would advise us not to rush hastily, but to allow
each verse, each word, each sound, to penetrate our hearts and minds
in order to reap its greatest possible benefit. Otherwise, we may
deprive ourselves of essential keys to unlocking truths buried deep
within us.

> Behold, your Lord said to the angels: "I am going to place a
vicegerent on earth." They said: "Will you place therein one who will
spread corruption and shed blood? While we celebrate your praises and
glorify your holiness?" He said: "Truly, I know what you do not know."
([2:30](https://quran.com/2/30){target="_quran"})

The opening scene is heaven as God informs the angels that He is about
to place man on earth. Adam, the first man, has not yet appeared. From
the verses that follow, it is clear that at this point in the story
Adam is free of any wrongdoing. Nevertheless, God plans to place him
(and his descendants
[6:165](https://quran.com/6/165){target="_quran"};
[27:62](https://quran.com/27/62){target="_quran"};
[35:39](https://quran.com/35/39){target="_quran"}) on earth in the
role of vicegerent or vicar (_khalifah_). There is no insinuation here
that earthly life is to serve as a punishment. The word _khalifah_
means "a vicar", "a delegate", "a representative", "a person
authorized to act for others". Therefore, it appears that man is meant
to represent and act on behalf of God in some sense.

The angels' reply is both fascinating and disturbing. In essence it
asks, "Why create and place on earth one who has it within his nature
to corrupt and commit terrible crimes? Why create this being, who will
be the cause and recipient of great suffering?" It is obvious that the
angels are referring here to the very nature of mankind, since Adam,
in the Qur'an, turns out to be one of God's elect and not guilty of
any major crime. The question is made all the more significant when we
consider who and from where it comes.

When we think of angels, we imagine peaceful, pure, and holy creatures
in perfect and joyous submission to God. They represent the model to
which we should aspire. In our daily speech, we reserve the word
"angel" for the noblest of our species. Mother Theresa is often called
an "angel of mercy" by the press. Of a person who does something very
kind we say, "He is such an angel!" When my wife and I look in on our
daughters at night, sleeping so beautifully and serenely, we remark to
each other, "Aren't they angels?"  Our image of an angel is of the
perfect human being. This is what gives the angels' question such
force, for it asks: "Why create this patently corrupt and flawed being
when it is within Your power to create _us_?" Thus they say: "While
_we_ celebrate your praises and glorify your holiness?" Their question
is given further amplification by the fact that it originates in
heaven, for what possible purpose could be served by placing man in an
environment where he could exercise freely his worst criminal
inclinations? All of these considerations culminate in the obvious
objection: Why not place man with a suitable nature in heaven from the
start? We are not even a single verse into the story of man and we
have already confronted our (the atheists') main complaint. And, it is
put in the mouths of the angels!

The verse ends not with an explanation, but a reminder of God's
superior knowledge, and hence, the implication that man's earthly life
is part of a grand design. Many western scholars have remarked that
the statement, "I know what you do not know," merely dismisses the
angel's question. However, as the sequence of passages will show, this
is not the case at all.

Our initial encounter with the Qur'an has been anything but pleasant;
it has been distressful and irritating. Either the author is
completely unaware of possible philosophical problems and objections,
or else he is deliberately provoking us with them! We are a mere
thirty-seven verses into the Qur'an and our anxiety and resentment has
been aroused to a fever pitch.  We ask, "Why indeed subject mankind to
earthly suffering? Why not remove us to heaven or place us there from
the first? Why must we struggle to survive? Why create us so
vulnerable and self-destructive? Why must we suffer broken hearts and
broken dreams, lost loves and lost youth, crises and catastrophes? Why
must we experience pains of birth and pains of death? Why?" we beg in
our frustration. "Why?" we plead in all our sorrow and
emptiness. "Why?" we insist in our anguish. "Why?!" we scream out to
the heavens. "Why?!" we plead with the angels. "If You are there and
You hear us, tell us, why create man?!!"

## And His Lord Turned toward Him {-}

We move now to verse thirty-one, where we find that the Qur'an
continues to explore the angels' question.

> And He taught Adam the names of all things; then He placed them
before the angels, and said, "Tell me their names if you are right."
([2:31](https://quran.com/2/31){target="_quran"})

Clearly, the angels' question is being addressed in this verse. Adam's
capacities for learning and acquiring knowledge, his ability to be
taught, are the focus of this initial response. The next verse
demonstrates the angels' inferiority in this respect. Special emphasis
is placed on man's ability to name, to represent by verbal symbols,
"all things" that enter his conscious mind: all his thoughts, fears,
and hopes, in short, all that he can perceive or conceive. This allows
man to communicate his experience and knowledge on a relatively high
level, as compared to the other creatures about him, and gives all
human learning a preeminent cumulative quality. In several places in
the Qur'an, this gift to mankind is singled out as one of the greatest
bounties bestowed on him by God.[^12]

[^12]: See p. 26.

> They said: Glory to you: we have no knowledge except what You taught
us, in truth it is you who are the Knowing, the
Wise. ([2:32](https://quran.com/2/32){target="_quran"})

In this verse, the angels plead their inability to perform such a
task, for, as they plainly state, it would demand a knowledge and
wisdom beyond their capacity. They maintain that its performance
would, of course, be easy for God, since His knowledge and wisdom is
supreme, but that the same could not be expected of them. In the next
passage, we discover that Adam possesses the level of intelligence
necessary to accomplish the task and hence, though his knowledge and
wisdom are less than God's, it is yet greater than the angels.

> He said: "O Adam! Tell them their names." When he had told them
their names, God said: "Did I not tell you that I know what is unseen
in the heavens and the earth and I know what you reveal and conceal?"
([2:33](https://quran.com/2/33){target="_quran"})

Here we have an emphatic statement that man's greater intellect
figures into an answer to the angels' question. We are informed that
God takes all into account, in particular, all aspects of the human
personality: man's potential for evil, which the angels' question
"reveals", and his complementary and related capacity for moral and
intellectual growth, which their question "conceals". To drive home
this point, the next verse has the angels demonstrate their
inferiority to Adam and shows that man's more complex personality
makes him a potentially superior being.

> And behold, We said to the angels, "Bow down to Adam" and they bowed
down. Not so Iblis: he refused and was proud: he was of the
rejecters. ([2:34](https://quran.com/2/34){target="_quran"})

We also find in this verse the birth of sin and temptation. The Qur'an
later informs us that Iblis (Satan) is of the _jinn_
([18:50](https://quran.com/18/50){target="_quran"}), a being created
of a smokeless fire
([55:15](https://quran.com/55/15){target="_quran"}) and who is
insulted at the suggestion that he should humble himself before a
creature made of "putrid clay"
([7:12](https://quran.com/7/12){target="_quran"};
[17:61](https://quran.com/17/61){target="_quran"};
[38:76](https://quran.com/38/76){target="_quran"}). Satan is portrayed
as possessing a fiery, consuming, and destructive nature. He allows
his passions to explode out of control and initiates a pernicious
rampage. We are often told that money is at the root of all evil, but
here the lesson appears to be that pride and self-centeredness is at
its core. Indeed, many terrible wrongs are committed for no apparent
material motive.

> And we said: "O Adam! Dwell you and your spouse in the garden and
eat freely there of what you wish, but come not near this tree for you
will be among the wrongdoers."
([2:35](https://quran.com/2/35){target="_quran"})

Thus the famous and fateful command. Yet, the tone of it seems
curiously restrained. There is no suggestion that the tree is in any
way special; it almost seems as if it were picked at random. Satan
will later tempt Adam with the promise of eternal life and "a kingdom
that never decays"
([20:121](https://quran.com/20/121){target="_quran"}), but this turns
out to be a complete fabrication on his part. There is not the
slightest hint that God is somehow threatened at the prospect of Adam
and his spouse violating the command; instead, He voices concern for
_them_, because then "_they_ will be among the wrongdoers".

This is probably an appropriate place to reflect on what we have
learned so far. We saw how God originally intended for man to have an
earthly life. We then observed a period of preparation during which
man is "taught" to use his intellectual gifts. Now, Adam and his
spouse are presented a choice, of apparently no great consequence,
except for the fact that it is made to be a moral choice. It thus
seems that man has gradually become -- or is about to become -- a
moral being.

> But Satan caused them to slip and expelled them from the state in
which they were. And we said: "Go you all down, some of you being the
enemies of others, and on earth will be your dwelling place and
provision for a
time. ([2:36](https://quran.com/2/36){target="_quran"})

Once again the Qur'an seems to have a penchant for understating
things. The Arabic verb _azalla_ means to cause someone to
unintentionally slip or lose his footing. But how can one of the most
terrible wrongs ever committed be described as a momentary "slip"? Yet
perhaps we are letting our own religious backgrounds, even though we
rejected them, distort our reading. Perhaps the Qur'an considers this
sin as nothing more than a temporary slip. After all, it is only a
tree! Its only significance may be that it signals a new stage in
man's development, that it causes man to depart from a previous state.

The words "some of you being the enemies of others" apparently refer
to all mankind and echo the angels' remark concerning man's earthly
strife.

Under normal circumstances, we would know now what to expect. We have
been terrified by it ever since we were children. It shook us from our
sleep and required our mothers to calm our fears and, unlike other
nightmares, it never went away when we awoke, because it was confirmed
by everyone we trusted. We know that there is about to be unleashed on
mankind a rage, a violence, a terror, the like of which has never been
known either before or since. Like a huge, thundering, black, and
terrifying storm cloud, looming on the horizon and heading straight
for us, mankind is about to be engulfed by an awesome fury. And when
the smoke clears, man will find himself sentenced, TO LIFE, on earth,
where he and all his descendants will suffer and struggle to survive
by their sweat and toil. There they will experience illness, agony,
and death. There they will suffer endless pain and torment and, in all
probability, more of the same and worse in the life to come.

And the WOMAN!!!! To her belong the greater punishment and
humiliation, for it was she who duped Adam with her beauty and her
charms. It was she who allied herself with Satan -- an alliance for
which Adam was, of course, no match. It was she who corrupted his
innocence and exposed his weaknesses. So it is she who will ache and
bleed monthly. It is she who will scream out in her labor pains. It is
she who will bare the brunt of greater humiliation and drudgery,
because the man will be made to rule over her, in spite of the fact
that he is obviously her intellectual inferior, since he was unequal
to her cleverness and guile.

So we wince and shudder as we turn to the terror we have always
known. We cringe and cower as we peek to the next verse.

> Then Adam received words from his Lord, and He turned to him
(mercifully). Truly He is Oft-returning, the
Merciful. ([2:37](https://quran.com/2/37){target="_quran"})

What is this? What is this talk of mercy and turning compassionately
towards man? Where is the passion, the jealousy, the anger, erupting
out of control?

In this verse, those that follow, and others in the Qur'an that relate
the same episode (see, for
example,[20:116-124](https://quran.com/20/116-124){target="_quran"},
the tone is first and foremost consoling and assuring. God immediately
pardons Adam and Eve, with no greater blame assigned to either of
them. Adam receives "words", which some commentators interpret to be
words of inspiration and others see as divine assurances and
promises. The next verse supports the latter viewpoint, while there
are others that include him in the community of prophets (for example,
[3:32](https://quran.com/3/32){target="_quran"}) which sustain the
first.

> We said: Go down from this state all of you together; and truly
there will come to you guidance from Me and whoever follows My
guidance, no fear shall come upon them, nor shall they
grieve. ([2:38](https://quran.com/2/38){target="_quran"})

The command issued in [2:36](https://quran.com/2/36){target="_quran"}
is repeated here, but this time with special emphasis put on God's
assurances and promises to mankind, thus further precluding the
interpretation that man's earthly life is a punishment. This explains
why the Qur'an has man remain on earth even though Adam and Eve are
immediately forgiven. Nonetheless, the Qur'an will insist, as we read
through it, that life serves definite aims and, as the next verse
warns, it has grave consequences and must be taken seriously.

> But those who are rejecters and give the lie to our signs, these are
the companions of the fire: they will abide in
it. ([2:39](https://quran.com/2/39){target="_quran"})

The story of Adam ends here to be taken up in bits and pieces later.
Many questions and problems have been raised, but we have obtained
only some clues and clarifications. This is another characteristic of
the Qur'an: It interweaves themes throughout the text, rather than
provide several distinct and complete discourses on various topics. In
this way it baits the reader, luring him or her into its design, so
that its different approaches are allowed to exert their influence
frequently and repeatedly. It would be naive of us to expect long
uninterrupted dissertations on metaphysics or theology, for such would
be understandable to few and of interest and inspiration to far
fewer. On the other hand, if the Qur'an is a guidance, as it claims to
be, then we should anticipate suggestions, guideposts, and touchstones
that help us along the way. Be assured, the Qur'an will not simply
translate us to our goal; it will provide directions at different
stages, but the traveling and the discovery will have to be ours,
because the questions we ask are not only about God -- they are about
ourselves as peculiar individuals and we are the only persons who have
real access to our souls. Thus, as the Qur'an might say, we must be
willing not only to search the horizons, but also our own selves,
until we know as much as we can grasp of the truth
([41:53](https://quran.com/41/53){target="_quran"}).

## "When Will God's Help Come?" {-}

Although the picture is still far from clear, some themes that invite
further reflection and elaboration have emerged. The most striking
fact that we observed is that the Qur'an does not maintain that life
on earth is a punishment. Long before Adam and Eve enter the story,
the angels raise the troublesome question: Why create man? A series of
verses supplies pieces of an answer. Man has a relatively higher
intelligence than other creatures. His nature is more complex and he
has a greater degree of personal freedom. Thus, he has not only
potential for growth in evil but, reciprocally, he has the potential
for growth in virtue. We witness a period of preparation, wherein man
learns to use his intellectual strengths. Adam and Eve are then ready
to become moral beings. They are presented with a somewhat innocuous
-- although from the standpoint of their development critical -- moral
choice. They slip and enter a moral phase in their existence, which is
symbolized in other Qur'anic passages that have the couple now
conscious of sexual morality and modesty
([7:19-25](https://quran.com/7/19-25){target="_quran"};
[20:120-123](https://quran.com/20/120-123){target="_quran"}). Thus they
depart from a state of ignorance, innocence, and bliss. Man's higher
intellect, freedom of choice, and growth potential will inevitably
involve him in conflict and travail.  The last of these is the focus
of the angels' question. As we continue along our journey, the Qur'an
will stress repeatedly these three features of the human venture:
reasoning, choice, and adversity. We will consider each separately.

### _Reason_ {-}

That the Qur'an gives a prominent place to reason in the attainment of
faith is well known and frequently mentioned by western
Islamicists. Many western scholars view this as a defect, because they
see faith and reason as being inherently incompatible. For example,
H. Lammens sarcastically states that the Qur'an "is not far from
considering unbelief as an infirmity of the human mind!" [^13] His
reaction, however, is more cultural and emotional than rational,
having its roots in the West's own struggle with religion and
reason. Yet not all western scholars are so cynical. While certainly
no advocate for Islam, Maxime Rodinson sees this aspect of the Qur'an
as somewhat in its favor and writes:

[^13]: H. Lammens,"Caracteristiquede Mohomet d'apres le Qoran",
_Recherches de science religieuse_, no. 20 (1930): 416-38.

> The Koran continually expounds the rational proofs of Allah's
omnipotence: the wonders of creation, such as the gestation of
animals, the movements of the heavenly bodies, atmospheric phenomena,
the variety of animal and vegetable life so marvelously well adapted
to men's needs. All those things "are signs (_ayat_) for those of
insight" ([3:187-190](https://quran.com/3/187-190){target="_quran"})
.... Repeated about fifty times in the Koran is the verb _'aqala_
which means "connect ideas together, reason, understand an
intellectual argument".  Thirteen times we come upon the refrain,
after a piece of reasoning: _a fa-la ta'qilun_ "have ye then no
sense?" ([2:41-44](https://quran.com/2/41-44){target="_quran"}, etc.)
The infidels, those who remain insensible to Muhammad's preaching, are
stigmatized as "a people of no intelligence," persons incapable of the
intellectual effort needed to cast off routine thinking
([5:63-58](https://quran.com/5/63-58){target="_quran"},
[5:102-103](https://quran.com/5/102-103){target="_quran"};
[10:42-43](https://quran.com/10/42-43){target="_quran"};
[22:45-46](https://quran.com/22/45-46){target="_quran"};
[9:14](https://quran.com/9/14){target="_quran"}). In this respect they
are like cattle
([2:166-171](https://quran.com/2/166-171){target="_quran"};
[5:44-46](https://quran.com/5/44-46){target="_quran"}). [^14]

[^14]: Maxime Rodinson, _Islam and Capitalism_ (Penguin Books, 1974),
    79-80.

The Qur'an insists that it contains signs for those who are "wise"
([2:269](https://quran.com/2/269){target="_quran"}), "knowledgeable"
([29:42-43](https://quran.com/29/42-43){target="_quran"}), "endowed
with insight" ([39:9](https://quran.com/39/9){target="_quran"}), and
"reflective" ([45:13](https://quran.com/45/13){target="_quran"}). Its
persistent complaint against its rejecters is that they refuse to make
use of their intellectual faculties and that they close their minds to
learning. The Qur'an asks almost incredulously: "Do they not travel
through the land, so that their hearts may thus learn wisdom?"
([22:44](https://quran.com/22/44){target="_quran"}), "Do they not
examine the earth?" ([26:7](https://quran.com/26/7){target="_quran"}),
"Do they not travel through the earth and see what was the end of
those before them?" ([30:9](https://quran.com/30/9){target="_quran"}),
"Do they not look at the sky above them?"
([50:6](https://quran.com/50/6){target="_quran"}), "Do they not look
at the camels, how they are made?"
([88:17](https://quran.com/88/17){target="_quran"}), "Have you not
watched the seeds which you sow?"
([56:63](https://quran.com/56/63){target="_quran"}).

Muslim school children throughout the world are frequently reminded of
the first five verses of the ninety-sixth _surah_:

> Read in the name of your Lord, who has created -- created man out of
a tiny creature that clings! Read and your Lord is the Most Bountiful
One, who has taught the use of the pen, taught man what he did not
know. ([96:1-5](https://quran.com/96/1-5){target="_quran"})

These verses are believed to comprise Muhammad's very first
revelation.  "Read!" It commands, as the skill of written
communication is presented as one of the great gifts to mankind,
because it is by use of the pen that God has taught man what he did
not or could not know. Here again, the Qur'an highlights man's unique
ability to communicate -- this time in writing -- and to collectively
learn from the insights and experiences of others.

Repetition is indicative of the importance given to certain topics. It
should be observed that the Arabic word for knowledge, _'ilm_, and its
derivatives appear 854 times in the Qur'an, placing it among the most
frequently occurring words. It should also be noted that in many of
its stories, where the Qur'an presents a debate between a believer and
disbeliever, the believer's stance is inevitably more rational and
logical than his opponent's.

### _Choice_ {-}

The Qur'an presents human history as a perennial struggle between two
opposing choices: to resist or to surrender oneself to God. It is in
this conflict that the scripture immerses itself and the reader; it
could be said to be the very crux of its calling. This choice must be
completely voluntary, for the Qur'an demands, "Let there be no
compulsion in religion -- the right way is henceforth clearly distinct
from error" ([2:256](https://quran.com/2/256){target="_quran"}). The
crucial point is not that one should come to know and worship God, but
that one should freely choose to know and worship God. Thus we find
the repeated declaration that God could have made all mankind rightly
guided, but it was within His purposes to do otherwise.

> Had He willed He could indeed have guided all of you.
  ([6:149](https://quran.com/6/149){target="_quran"})

> Do not the believers know that, had God willed, He could have guided
all mankind? ([13:31](https://quran.com/13/31){target="_quran"})

> And if We had so willed, We could have given every soul its
guidance. ([32:13](https://quran.com/32/13){target="_quran"})

> Had God willed He could have made you all one community. But that He
may try you by that which He has given you. So vie with one another in
good works. Unto God you will all return, and He will inform you of
that wherein you
differ. ([5:48](https://quran.com/5/48){target="_quran"})

The Qur'an categorically affirms that God is not diminished nor
threatened by our choices, yet they do carry grave consequences for
the individual, as the primary beneficiary of a good deed and the
primary casualty of an evil act is the doer.

> Enlightenment has come from your God; he who sees does so to his own
good, he who is blind is so to his own hurt.
([6:104](https://quran.com/6/104){target="_quran"})

> Indeed they will have squandered their own selves, and all their
false imagery will have forsaken
them. ([7:53](https://quran.com/7/53){target="_quran"})

> And they did no harm against Us, but [only] against their own selves
did they sin. ([7:160](https://quran.com/7/160){target="_quran"})

> And so it was not God who wronged them, it was they who wronged
themselves. ([9:70](https://quran.com/9/70){target="_quran"})

> And whosoever is guided, is only (guided) to his own gain, and if
any stray, say: "I am only a warner."
([27:92](https://quran.com/27/92){target="_quran"})

> And if any strive, they do so for their own selves: For God is free
of all need from
creation. ([29:6](https://quran.com/29/6){target="_quran"})

> We have revealed to you the book with the truth for mankind. He who
lets himself be guided does so to his own good; he who goes astray
does so to his own
hurt. ([39:41](https://quran.com/39/41){target="_quran"}) (Also see
([10:108](https://quran.com/10/108){target="_quran"};
[17:15](https://quran.com/17/15){target="_quran"};
[27:92](https://quran.com/27/92){target="_quran"}).)

These statements are hardly easy on the reader. At first glance they
seem to indicate a detachment from and indifference to man's
situation.  Philosophically, such a stance may be consistent with
God's transcendence, but only at the expense of attempting any
possible relationship with God.  Yet such an interpretation would be
inappropriately severe. The Qur'anic God is anything but impartial to
mankind's condition. He sends prophets, answers prayers
([2:186](https://quran.com/2/186){target="_quran"};
[3:195](https://quran.com/3/195){target="_quran"}), and intervenes in
and manipulates the human drama, as in the Battle of Badr
([3:13](https://quran.com/3/13){target="_quran"};
[8:5-19](https://quran.com/8/5-19){target="_quran"};
[8:42-48](https://quran.com/8/42-48){target="_quran"}). All is under
His authority, and nothing takes place without His allowing it
([4:78-79](https://quran.com/4/78-79){target="_quran"}).

The Qur'an's "most beautiful names" of God imply an intense
involvement in the human venture. These names, such as The Merciful,
The Compassionate, The Forgiving, The Giving, The Loving, The Creator,
etc., reveal a God that creates men and women in order to relate to
them on an intensely personal level, on a level higher than with the
other creatures known to mankind, not out of a psychological or
emotional need but because this is the very essence of His
nature. Therefore, we find that the relationship between the sincere
believer and God is characterized consistently as a bond of love. God
loves the good-doers
([2:195](https://quran.com/2/195){target="_quran"};
[3:134](https://quran.com/3/134){target="_quran"};
[3:148](https://quran.com/3/148){target="_quran"};
[5:13](https://quran.com/5/13){target="_quran"};
[5:93](https://quran.com/5/93){target="_quran"}), the repentant
([2:222](https://quran.com/2/222){target="_quran"}), those that purify
themselves ([2:222](https://quran.com/2/222){target="_quran"};
[9:108](https://quran.com/9/108){target="_quran"}, the God-conscious
[3:76](https://quran.com/3/76){target="_quran"};
[9:4](https://quran.com/9/4){target="_quran"};
[9:7](https://quran.com/9/7){target="_quran"}), the patient
([3:146](https://quran.com/3/146){target="_quran"}), those that put
their trust in Him
([3:159](https://quran.com/3/159){target="_quran"}), the just
([5:42](https://quran.com/5/42){target="_quran"};
[49:9](https://quran.com/49/9){target="_quran"};
[60:8](https://quran.com/60/8){target="_quran"}), and those who fight
in His cause ([61:4](https://quran.com/61/4){target="_quran"}). And
they, in turn, love God.

> Yet there are men who take others besides God as equal (with God),
loving them as they should love God. But those who believe love God
more ardently. ([2:165](https://quran.com/2/165){target="_quran"})

> Say: "If you love God, follow me, and God will love you, and forgive
you your faults; for God is The Forgiving, The
Merciful. ([3:31](https://quran.com/3/31){target="_quran"})

> O you who believe! If any from among you should turn back from his
faith, then God will assuredly bring a people He loves and who love
Him. ([5:54](https://quran.com/5/54){target="_quran"})

On the other hand, tyrants, aggressors, the corrupt ones, the guilty,
rejecters of faith, evil-doers, the arrogant ones, transgressors, the
prodigal, the treacherous, and the unjust will not experience this
relationship of love
([2:190](https://quran.com/2/190){target="_quran"};
[2:205](https://quran.com/2/205){target="_quran"};
[2:276](https://quran.com/2/276){target="_quran"};
[3:32](https://quran.com/3/32){target="_quran"};
[3:57](https://quran.com/3/57){target="_quran"};
[3:140](https://quran.com/3/140){target="_quran"};
[4:36](https://quran.com/4/36){target="_quran"};
[4:107](https://quran.com/4/107){target="_quran"};
[5:64](https://quran.com/5/64){target="_quran"};
[5:87](https://quran.com/5/87){target="_quran"};
[6:141](https://quran.com/6/141){target="_quran"};
[7:31](https://quran.com/7/31){target="_quran"};
[7:55](https://quran.com/7/55){target="_quran"};
[8:58](https://quran.com/8/58){target="_quran"};
[16:23](https://quran.com/16/23){target="_quran"};
[22:38](https://quran.com/22/38){target="_quran"};
[28:76](https://quran.com/28/76){target="_quran"};
[8:77](https://quran.com/8/77){target="_quran"};
[30:45](https://quran.com/30/45){target="_quran"};
[31:18](https://quran.com/31/18){target="_quran"};
[42:40](https://quran.com/42/40){target="_quran"};
[57:23](https://quran.com/57/23){target="_quran"}).

There are Muslim and non-Muslim scholars who view God in the Qur'an as
virtually indifferent to humanity, establishing the cosmos with fixed
laws of cause and effect in all spheres (physical, psychological,
spiritual, etc.), and then setting it to run subject to them. Others
have seen Him as so completely involved in and in control of creation
that all is totally determined, even our choices. Many believe that
shades of both viewpoints are present and that they may be
irreconcilable. The latter is perhaps closest to the truth, although
the irreconcilability is not necessary. Certainly, the Qur'an
maintains God's absolute sway over all creation, which He ceaselessly
and continuously sustains, maintains, and influences. Nothing exists
or takes place without His permission. Yet He empowers us with the
ability to make choices, act them out, and see them, most often, to
their expected conclusions. In fact, He frequently leads us to such
critical choices. In particular, He allows and enables us to make
decisions that are detrimental to ourselves and others:

> Say: "All things are from God." But what has come to these people,
that they fail to understand a single fact?  Whatever good befalls you
is from God, but whatever evil befalls you, is from
yourself. ([4:78-79](https://quran.com/4/78-79){target="_quran"})

The assertion here is that our ability to experience true benefit or
harm comes from God, but to do real injury to ourselves, in an
ultimate and spiritual sense, depends on our actions and decisions,
which God has empowered us to make.

As noted above, our acts and choices in no way threaten God and it is
the individual who gains or loses by them. However, on the collective
level, the Qur'an shows that God intends to produce through this
earthly experience persons that share a bond of love with Him. While
any individual may or may not pursue this, the Qur'an acknowledges
that there will definitely be people who will, and their development
is apparently the very object of man's earthly life
([15:39-43](https://quran.com/15/39-43){target="_quran"};
[17:64-65](https://quran.com/17/64-65){target="_quran"}).

Our vision is still quite blurry, but it seems somehow a little
clearer than when we started, although we still have far to
go. Questions persist about the need for this earthly life as well as
the roles of human choice, intelligence, and suffering in the creation
of individuals. It also feels as if we are slipping into the difficult
topic of predestination. We will reserve that subject for the end of
this chapter, for it will take us too far afield at this
stage. However, we need to discuss one more Qur'anic statement that
relates to divine and human will, because it is very often
misunderstood and has a strong bearing on the theme of human choice.

The Qur'an frequently states that God "allows to stray whom He will,
and guides whom He will"
([2:26](https://quran.com/2/26){target="_quran"};
[4:88](https://quran.com/4/88){target="_quran"};
[4:143](https://quran.com/4/143){target="_quran"};
[6:39](https://quran.com/6/39){target="_quran"};
[7:178](https://quran.com/7/178){target="_quran"};
[7:186](https://quran.com/7/186){target="_quran"};
[3:27](https://quran.com/3/27){target="_quran"};
[14:4](https://quran.com/14/4){target="_quran"};
[16:93](https://quran.com/16/93){target="_quran"};
[35:8](https://quran.com/35/8){target="_quran"};
[39:23](https://quran.com/39/23){target="_quran"};
[39:36](https://quran.com/39/36){target="_quran"};
[40:33](https://quran.com/40/33){target="_quran"};
[74:31](https://quran.com/74/31){target="_quran"}). This phrase is
typically rendered in most English interpretations as "God misleads
whom He will, and guides whom He will". Grammatically, both renderings
are possible, because the verb _adalla_ could be used in either
sense. It could mean either "to let someone or something stray
unguided", or equally, "to make someone or something lose his/its
way". Ignaz Goldziher, the famous Orientalist and scholar of Arabic,
strongly argues in favor of the first rendition. He writes,

> Such statements do not mean that God directly leads the latter into
error. The decisive verb (_adalla_) is not, in this context, to be
understood as "lead astray", but rather as "allow to go astray", that
is, not to care about someone, not to show him the way out of his
predicament "We let them (_nadharuhum_) stray in disobedience"
([6:110](https://quran.com/6/110){target="_quran"}). We must imagine a
solitary traveler in the desert: that image stands behind the Qur'an's
manner of speaking about guidance and error. The traveler wanders,
drifts in limitless space, on the watch for his true destination and
goal. Such a traveler is man on the journey of life. [^15]

[^15]: Ignaz Go1dziher, _Introduction to Islamic Theology and Law_
(Princeton: Princeton University Press, 1981), 79-80.

His observation is given further support by the fact that almost all
of the statements in the Qur'an of this nature are immediately
preceded or followed by others that assert that God guides or refuses
to guide someone according to his/her choices and predisposition. We
find that "God does not guide the unjust ones", "God does not guide
the transgressors", and God guides aright those who "listen", are
"sincere", and are "God-conscious"
([2:26](https://quran.com/2/26){target="_quran"},
[2:258](https://quran.com/2/258){target="_quran"},
[2:264](https://quran.com/2/264){target="_quran"};
[3:86](https://quran.com/3/86){target="_quran"};
[5:16](https://quran.com/5/16){target="_quran"},
[5:51](https://quran.com/5/51){target="_quran"},
[5:67](https://quran.com/5/67){target="_quran"},
[5:108](https://quran.com/5/108){target="_quran"};
[6:88](https://quran.com/6/88){target="_quran"},
[6:144](https://quran.com/6/144){target="_quran"};
[9:19](https://quran.com/9/19){target="_quran"},
[9:21](https://quran.com/9/21){target="_quran"},
[9:37](https://quran.com/9/37){target="_quran"},
[9:80](https://quran.com/9/80){target="_quran"},
[9:109](https://quran.com/9/109){target="_quran"};
[12:52](https://quran.com/12/52){target="_quran"};
[3:27](https://quran.com/3/27){target="_quran"};
[16:37](https://quran.com/16/37){target="_quran"},
[16:107](https://quran.com/16/107){target="_quran"};
[28:50](https://quran.com/28/50){target="_quran"};
[39:3](https://quran.com/39/3){target="_quran"};
[40:28](https://quran.com/40/28){target="_quran"};
[42:13](https://quran.com/42/13){target="_quran"};
[46:10](https://quran.com/46/10){target="_quran"};
[47:8](https://quran.com/47/8){target="_quran"};
[61:7](https://quran.com/61/7){target="_quran"};
[62:5](https://quran.com/62/5){target="_quran"};
[3:6](https://quran.com/3/6){target="_quran"}).  Thus we find that
"when they went crooked, God bent their hearts crooked"
([61:5](https://quran.com/61/5){target="_quran"}). This demonstrates
that receiving or not receiving guidance is affected by sincerity,
disposition and willingness; it recalls the saying of Muhammad: "When
you approach God by an arm's length, He approaches you by two, and if
you come to Him walking, He comes to you running." [^16] Therefore,
the phrase "God allows to stray whom He wills" illumines what we have
already concluded: while God, according to the Qur'an, could guide all
mankind uniformly, He has other purposes and hence does not.  Instead,
He has created man with a unique and profound ability to make moral
decisions and He monitors, influences, and guides each individual's
moral and spiritual development in accordance with them.

[^16]: From _Sahih at Bukhari_ as translated in Imam Nawawi, _Riyad at
Salihin_, trans. by Muhammad ZafrullahKhan (London: Curzon Press Ltd.,
1975), 28.

### _Suffering_ {-}

The great divide between theist and atheist is their reactions to
human suffering. Often the first views it as either deserved or an
impenetrable mystery, while the second sees it as unnecessary and
inexcusable.  The Qur'an advocates neither viewpoint. Trial and
tribulation are held to be inevitable and essential to human
development and both the believer and unbeliever will experience them.

> Most assuredly We will try you with something of danger, and hunger,
and the loss of worldly goods, of lives and the fruits of your
labor. But give glad tidings to those who are patient in adversity --
who when calamity befalls them, say, "Truly unto God do we belong and
truly, unto him we shall
return. ([2:155](https://quran.com/2/155){target="_quran"})

> Do you think that you could enter paradise without having suffered
like those who passed away before you? Misfortune and hardship befell
them, and so shaken were they that the Messenger and the believers
with him, would exclaim, "When will God's help come?" Oh truly, God's
help is always
near. ([2:214](https://quran.com/2/214){target="_quran"})

> You will certainly be tried in your possessions and
> yourselves. ([3:186](https://quran.com/3/186){target="_quran"})

> And if we make man taste mercy from Us, then withdraw it from him,
he is surely despairing, ungrateful. And if we make him taste a favor
after distress has afflicted him, he says: The evils are gone away
from me. Truly he is exultant, boastful; except those who are
persevering and do good. For them is forgiveness and a great reward.
([11:9-11](https://quran.com/11/9-11){target="_quran"})

> Every soul must taste of death. And we try you with calamity and
prosperity, [both] as a means of trial. And to Us you are
returned. ([21:35](https://quran.com/21/35){target="_quran"})

> O man! Truly you've been toiling towards your Lord in painful toil
-- but you shall meet Him!
([84:6](https://quran.com/84/6){target="_quran"})

Man, however, does not grow only through patient suffering, but also
by striving and struggling against hardship and adversity. This
explains why _jihad_ is such a key concept in the Qur'an. Often
translated as "holy war", the word _jihad_ literally means "a
struggle", "a striving", "an exertion", or "a great effort". It may
include fighting for a just cause, but it has a more general
connotation as the associated verbal noun of _jahada_, "to toil", "to
weary", "to struggle", "to strive after", "to exert oneself". The
following verses, revealed in Makkah before Muslims were involved in
combat, bring out this more general sense.

> And those who strive hard (_jahadu_) for Us, We shall certainly
guide them in Our ways, and God is surely with the doers of
good. ([29:69](https://quran.com/29/69){target="_quran"})

> And whoever strives hard (_jahada_) strives (_yujahidu_) for his
self, for God is Self-Sufficient, above the need of the
worlds. ([29:6](https://quran.com/29/6){target="_quran"})

> And strive hard (_jahidu_) for God with due striving
> (_jihadihi_). ([22:78](https://quran.com/22/78){target="_quran"})

> So obey not the unbelievers and strive against them (_jahidhum_) a
mighty striving (_jihadan_) with it [the
Qur'an]. ([25:52](https://quran.com/25/52){target="_quran"})

The last verse occurs in a passage that encourages Muslims to make use
of the Qur'an when they argue with disbelievers.

The Qur'an's attitude towards suffering and adversity is not passive
and resigned, but positive and dynamic. The believers are told that
they will surely suffer and to be patient and persevering in times of
hardship, but they are also to look forward and seek opportunities to
improve their situation and rectify existing wrongs. They are told
that while the risks and struggle may be great, the ultimate benefit
and reward will be much greater
([2:218](https://quran.com/2/218){target="_quran"};
[3:142](https://quran.com/3/142){target="_quran"};
[4:95-96](https://quran.com/4/95-96){target="_quran"};
[8:74](https://quran.com/8/74){target="_quran"};
[9:88-89](https://quran.com/9/88-89){target="_quran"};
[6:110](https://quran.com/6/110){target="_quran"};
[29:69](https://quran.com/29/69){target="_quran"}).

> Those who believed and fled their homes, and strove hard in God's
way with their possessions and their selves are much higher in rank
with God. And it is these -- they are the
triumphant. ([9:20](https://quran.com/9/20){target="_quran"})

Life was never meant to be easy. The Qur'an refers to a successful
life as an "uphill climb," a climb that most will avoid.

> We certainly have created man to face distress. Does he think that
no one has power over him? He will say: I have wasted much
wealth. Does he think that no one sees him?  Have We not given him two
eyes, and a tongue and two lips and pointed out to him the two
conspicuous ways? But he attempts not the uphill climb; and what will
make you comprehend the uphill climb? [It is] to free a slave, or to
feed in a day of hunger an orphan nearly related, or the poor one
lying in the dust. Then he is of those who believe and exhort one
another to patience and exhort one another to
mercy. ([90:4-17](https://quran.com/90/4-17){target="_quran"})

## Wishful Thinking {-}

"Why create man?" The angels' question echoes through our
reflections. We can at least attempt a partial explanation based on
what we have learned from the Qur'an so far.

It seems that God, in accordance with His attributes, intends to make
a creature that can experience His being (His mercy, compassion, love,
kindness, beauty, etc.) in an intensely personal way and at a level
higher than the other beings known to mankind. The intellect and will
that man has been given, together with the strife and struggle that he
will surely face on earth, somehow contribute to the development of
these individuals, this subset of humanity that will be bound to God
by love.

We need to travel on and delve deeper in this direction in order to
gain greater insight. But before we do, we should consider the
possibility that we may be deluding ourselves. By this I mean that we
may have been reading into the Qur'an something that is not really
there; we may have been projecting onto the scripture our own personal
conflict, one that the Qur'an never insists on explicitly nor even
intentionally raises the issue of an ultimate purpose behind the
creation of man. Yet here again we meet with the persistently
provocative method of the scripture. Just when we are prepared to
doubt our first impressions and to revert to the familiar and
comforting corner of cynicism from which we have come, the Qur'an
reopens the topic.

> Those [are believers] who remember God standing and sitting and
lying down and reflect upon the creation of the heavens and the earth
[and say]: Our Lord, You did not create all this in
vain. ([3:191](https://quran.com/3/191){target="_quran"})

> We have not created the heaven and the earth and whatever is between
them in sport. If We wish to take a sport, We could have done it by
Ourselves -- if We were to do that at
all. ([21:16-17](https://quran.com/21/16-17){target="_quran"})

> Do you think that We created you purposely and that you will not be
returned to Us? The true Sovereign is too exalted above
that. ([23:115](https://quran.com/23/115){target="_quran"})

> We did not create the heavens and the earth and all that is between
them, in play. ([44:38](https://quran.com/44/38){target="_quran"})

So for us there is no easy escape. The Qur'an apparently will not back
out of the challenge. It is up to us to either continue on in this
search or to resign and avoid a decisive engagement. Our numbers now
are almost surely less than when we started. For those willing to
continue, we should consider what would be the next natural
step. Since the Qur'an undoubtedly claims that life has a reason and
since, as we observed, it has to do with the nurturing of a certain
relationship between God and man, it would seem quite appropriate to
seek more information about the nature of man and what the Qur'an
requires of him as well as about the attributes of God and how mankind
is affected by them.

## Except Those Who Have Faith and Do Good {-}

The key to success in this life and the hereafter is stated so
frequently and formally in the Qur'an that no serious reader can miss
it. However, the utter simplicity of the dictum may cause one to
disregard it, because it seems to ignore the great questions and
complexities of life. The Qur'an maintains that only "those who have
faith and do good" (in Arabic: _allathina aaminu wa 'amilu al
saalihaat_) will benefit from their earthly lives
([2:25](https://quran.com/2/25){target="_quran"};
[2:82](https://quran.com/2/82){target="_quran"};
[2:277](https://quran.com/2/277){target="_quran"};
[4:57](https://quran.com/4/57){target="_quran"};
[4:122](https://quran.com/4/122){target="_quran"};
[5:9](https://quran.com/5/9){target="_quran"};
[7:42](https://quran.com/7/42){target="_quran"};
[10:9](https://quran.com/10/9){target="_quran"};
[11:23](https://quran.com/11/23){target="_quran"};
[13:29](https://quran.com/13/29){target="_quran"};
[14:23](https://quran.com/14/23){target="_quran"};
[18:2](https://quran.com/18/2){target="_quran"};
[18:88](https://quran.com/18/88){target="_quran"};
[8:107](https://quran.com/8/107){target="_quran"};
[19:60](https://quran.com/19/60){target="_quran"};
[19:96](https://quran.com/19/96){target="_quran"};
[20:75](https://quran.com/20/75){target="_quran"};
[20:82](https://quran.com/20/82){target="_quran"};
[20:112](https://quran.com/20/112){target="_quran"};
[21:94](https://quran.com/21/94){target="_quran"};
[22:14](https://quran.com/22/14){target="_quran"};
[22:23](https://quran.com/22/23){target="_quran"};
[2:50](https://quran.com/2/50){target="_quran"};
[22:56](https://quran.com/22/56){target="_quran"};
[24:55](https://quran.com/24/55){target="_quran"};
[25:70-71](https://quran.com/25/70-71){target="_quran"};
[26:67](https://quran.com/26/67){target="_quran"};
[28:80](https://quran.com/28/80){target="_quran"};
[29:7](https://quran.com/29/7){target="_quran"};
[29:9](https://quran.com/29/9){target="_quran"};
[29:58](https://quran.com/29/58){target="_quran"};
[30:15](https://quran.com/30/15){target="_quran"};
[30:45](https://quran.com/30/45){target="_quran"};
[31:8](https://quran.com/31/8){target="_quran"};
[32:19](https://quran.com/32/19){target="_quran"};
[34:4](https://quran.com/34/4){target="_quran"};
[34:37](https://quran.com/34/37){target="_quran"};
[35:7](https://quran.com/35/7){target="_quran"};
[38:24](https://quran.com/38/24){target="_quran"};
[41:8](https://quran.com/41/8){target="_quran"};
[42:22](https://quran.com/42/22){target="_quran"};
[42:23](https://quran.com/42/23){target="_quran"};
[2:26](https://quran.com/2/26){target="_quran"};
[45:21](https://quran.com/45/21){target="_quran"};
[45:30](https://quran.com/45/30){target="_quran"};
[47:2](https://quran.com/47/2){target="_quran"};
[47:12](https://quran.com/47/12){target="_quran"};
[48:29](https://quran.com/48/29){target="_quran"};
[64:9](https://quran.com/64/9){target="_quran"};
[65:11](https://quran.com/65/11){target="_quran"};
[84:25](https://quran.com/84/25){target="_quran"};
[85:11](https://quran.com/85/11){target="_quran"};
[5:6](https://quran.com/5/6){target="_quran"};
[98:7](https://quran.com/98/7){target="_quran"};
[103:3](https://quran.com/103/3){target="_quran"}). This statement and
very similar ones occur so often that it warrants careful analysis.

_Wa 'amilu al saalihaat_ (and do good). The verb _'amila_ means "to
do", "to act", "to be active", "to work", or "to make". It implies
exertion and effort. Thus the associated noun _'amal_ (pl. _a'maal_)
means "action", "activity", "work" or "labor", as in the verse, "I
waste not the labor (_'amala_) of any that labors (_'amilin_)" (3:
195). The noun _al saalihaat_ is the plural of _saalih_, which means
"a good or righteous act". But this definition does not bring out its
full meaning. The verbs _salaha_ and _aslaha_, which come from the
same Arabic root, mean "to act rightly and properly", "to put things
in order", "to restore", "to reconcile", and "to make or foster
peace". Hence the noun _sulh_ means "peace", "reconciliation",
"settlement" and "compromise".  Therefore, the phrase _'amilu al
saalihaat_ ("do good") refers to those who persist in striving to set
things right; to restore harmony, peace, and balance.

From the Qur'an's many exhortations and its descriptions of the acts
and types of individuals loved by God, it is not difficult to compose
a partial list of "good works". Not unexpectedly, it will consist of
those acts and attributes that are universally recognized as
virtuous. One should show compassion
([2:83](https://quran.com/2/83){target="_quran"};
[2:215](https://quran.com/2/215){target="_quran"};
[69:34](https://quran.com/69/34){target="_quran"}), be merciful
([90:17](https://quran.com/90/17){target="_quran"}), forgive others
([42:37](https://quran.com/42/37){target="_quran"};
[45:14](https://quran.com/45/14){target="_quran"};
[64:14](https://quran.com/64/14){target="_quran"}), be just
([4:58](https://quran.com/4/58){target="_quran"};
[6:152](https://quran.com/6/152){target="_quran"};
[6:90](https://quran.com/6/90){target="_quran"}), protect the weak
([4:127](https://quran.com/4/127){target="_quran"};
[6:152](https://quran.com/6/152){target="_quran"}), defend the
oppressed ([4:75](https://quran.com/4/75){target="_quran"}), seek
knowledge and wisdom
([20:114](https://quran.com/20/114){target="_quran"};
[22:54](https://quran.com/22/54){target="_quran"}), be generous
([2:177](https://quran.com/2/177){target="_quran"};
[23:60](https://quran.com/23/60){target="_quran"};
[0:39](https://quran.com/0/39){target="_quran"}), truthful
([3:17](https://quran.com/3/17){target="_quran"};
[33:24](https://quran.com/33/24){target="_quran"};
[33:35](https://quran.com/33/35){target="_quran"};
[49:15](https://quran.com/49/15){target="_quran"}), kind
([4:36](https://quran.com/4/36){target="_quran"}), and peaceful
([8:61](https://quran.com/8/61){target="_quran"};
[25:63](https://quran.com/25/63){target="_quran"};
[47:35](https://quran.com/47/35){target="_quran"}), and love others
([19:86](https://quran.com/19/86){target="_quran"}).

> Truly those who believe and do good will the Most Merciful endow
with love and to this end have We made this easy to understand in your
own tongue, so that you might convey a glad tiding to the
God-conscious and warn those given to
contention. ([19:86](https://quran.com/19/86){target="_quran"})

One should teach and encourage others to practice these virtues
([90:17](https://quran.com/90/17){target="_quran"};
[103:3](https://quran.com/103/3){target="_quran"}) and, by
implication, learn and grow in them as well. The stories of the
prophets have God's messengers bidding their communities and families
to adopt such ethics, although many of them remain contemptuous.

It is not surprising that the Qur'an upholds the so-called golden
rule.  Many do feel that it is better to give than to receive, to be
truthful rather than to live a lie, to love rather than to hate, to be
compassionate rather than to ignore the suffering of others, for such
experiences give life depth and beauty. I believe that in the winters
of our lives, our past worldly or material achievements will seem less
important to us than the relationships we had, loves and friendships
that we shared, and times we spent giving of ourselves and doing good
to others. In the end, according to the Qur'an, these are what endure.

> But the things that endure -- the good deeds -- are, with your Lord,
better in reward and better in
hope. ([18:46](https://quran.com/18/46){target="_quran"})

> And God increases the guided in guidance. And the deeds that endure
-- the good deeds -- are, with your Lord, better in reward and yield
better returns. ([19:76](https://quran.com/19/76){target="_quran"})

These are the themes of songs, poems, novels, plays, and films not
only because of their sentimental appeal, but because they are part of
our collective human experience and wisdom. Some would say that life
is really not about taking, but about giving and sharing, and that
this is what gives life meaning and purpose. The Muslim, however,
would not fully agree. If human intellectual, moral, and emotional
evolution was the sole purpose of life, then belief in God might be
helpful, but not entirely necessary, for a humanistic ideology may
suffice. But the Qur'an does not state that the successful in life are
only "those who do good"; rather, they are only those who unite faith
with righteous living, those who "have faith and do good".

_Illa-l lathina aamanu_ (except those who believe). The verb _aamana_
means "to be faithful", "to trust", "to have confidence in", and "to
believe in". It is derived from the Arabic root, _AMN_, which is
associated with the ideas of safety, security, and peace. Thus _amina_
means "to be secure", "to feel safe", "to trust"; _amn_ means
"safety", "peace", "protection"; _amaan_ means "safety", "shelter",
"peace", and "security". The translation of _aaminu_ as "believe" is
somewhat misleading, because in modern times it is usually used in the
sense of "to hold an opinion" or "to accept a proposition or statement
as true". The Arabic word has stronger emotional and psychological
content, for "those who believe" implies more than an acceptance of an
idea; it connotes a personal relationship and commitment and describes
those who find security, peace, and protection in God and who are in
turn faithfully committed to Him.

Like the phrase we are analyzing, the Qur'an maintains the utter
indivisibility between faith and good works. The mention of the first
is almost always conjoined to the second. Faith should inspire
righteous deeds, which, in turn, should nurture a more profound
experience of faith, which should incline one to greater acts of
goodness, and so on, with each a function of the other, rising in a
continuous increase. From this viewpoint, all of our endeavors acquire
a potential unity of purpose: ritualistic, spiritual, humanitarian,
and worldly activity are all brought into the domain of worship. Good
deeds become simultaneously God-directed and man-directed acts. For
example, the spending of one's substance on others becomes an
expression of one's love of God.

> But piety is to ... spend of your substance out of love for
Him. ([2:177](https://quran.com/2/177){target="_quran"})

Hence the relationship between God and man is inextricably bound to
man's relationship with fellow man.

Repeatedly, we come upon the Qur'anic exhortation, "Establish salah
[ritual prayers] and pay zakah [the annual financial tax]." We
normally think of the first as God-directed and the second as
community, and hence man, directed. While this may be so, the line
between them is extremely faint in Islam, for both are ritual
obligations and both require and contribute to a high level of
community discipline and cohesion.

Many a non-Muslim has been impressed with the synchronous, almost
military, precision of a Muslim congregation in prayer. At the call to
the prayer, the congregation quickly arranges itself in tight
formation, with no one possessing a fixed or privileged position, so
that even the prayer leader is frequently elected on the spot from
those present. In this way, the prayer becomes not only a powerful
spiritual exercise, but, secondarily, also trains the community in
leadership, organization, cooperation, equality, and brotherhood. The
physical and hygienic advantages of preparing for and performing the
ritual prayer have also been noted frequently by outsiders. This is
not to say that Muslims would list these gains as the primary benefits
of prayer -- indeed they would not -- but it does exemplify how the
spiritual and worldly intersect and complement each other in Islam.

The ritual of zakah illustrates the same point but from the reverse
angle. It is the yearly tax -- something like a social security tax --
on a Muslim's wealth, which is distributed to the poor and needy and
others as stipulated in the Qur'an
([9:60](https://quran.com/9/60){target="_quran"}). The social concerns
behind this tax are obvious, but the Qur'an underlines its personal
and spiritual sides as well.  The word zakah, which means "alms" or
"charity", is associated with the Arabic verbs _zakka_ and _tazakka_,
which mean "to purify" or "to cleanse".  Muslims have long understood
that through its payment one may attain to higher levels of spiritual
purity. This is not a coincidental or forced association, because the
Qur'an clearly makes the connection between almsgiving and
self-purification.

> So take of their wealth alms, so that you might purify
(_tuzakkeehim_) and cleanse
them. ([9:103](https://quran.com/9/103){target="_quran"})

> But the most devoted to God shall be removed far from it: those who
spend their wealth to purify (_yatazakka_)
themselves. ([92:18](https://quran.com/92/18){target="_quran"})

The Qur'an's recurring summons to establish salah and pay zakah is
indicative of its general attitude towards faith and good deeds: They
are interconnected and mutually enriching. The ultimate goal is to
perfect harmony between both types of activities, as each is
indispensable to our complete development. Thus, giving of oneself
strengthens the experience of faith, or, as the Qur'an says, spending
in God's way and doing good brings one nearer to God and His mercy.

> And some of the desert Arabs are of those who believe in God and the
Last Day and consider what they spend as bringing them nearer to God
and obtaining the prayers of the Messenger. Truly they bring them
nearer [to Him]; God will bring them into His
mercy. ([9:99](https://quran.com/9/99){target="_quran"})

> And it is not your wealth nor your children that bring you near to
Us in degree, but only those who believe and do good, for such is a
double reward for what they do, and they are secure in the highest
places. ([34:37](https://quran.com/34/37){target="_quran"})

The vision of the "face of God" refers to the intense mystical
encounter obtained in the hereafter by those who attain the highest
levels of spirituality and goodness. Here too the Qur'an connects this
divine vision with our concern and responsibility towards others.

> So give what is due to kindred, the needy and the wayfarer. That is
best for those who seek the face of God, and it is these, they are the
successful. ([30:39](https://quran.com/30/39){target="_quran"})

As these verses show, virtuous acts augment faith and spirituality.
More than acceptance of dogma or a state of spiritual consciousness,
faith in Islam is comprehensive, an integrated outlook and way of
living that incorporates all aspects of human nature and that
increases with the level of giving and self-sacrifice.

> By no means shall you attain piety unless you give of that which you
love. And whatever you give, God surely knows
it. ([3:92](https://quran.com/3/92){target="_quran"})

> Those who responded to the call of God and the Messenger after
misfortune had befallen them -- for such among them who do good and
refrain from wrong is a great reward. Men said to them: surely people
have gathered against you, so fear them; but this increased them in
faith, and they said: God is sufficient for us and He is an excellent
guardian. ([3:172-173](https://quran.com/3/172-173){target="_quran"})

> When the believers saw the confederate forces, they said: "This is
what God and His messenger had promised us, and God and His messenger
told us what was true." And it only increased them in faith and in
submission. ([33:22](https://quran.com/33/22){target="_quran"})

Conversely, the spiritual experiences of faith should intensify one's
commitment to goodness:

> And those who give what they give while their hearts are full of awe
that to their Lord they must return -- _These hasten to every good
work and they are foremost in them._
([23:60-61](https://quran.com/23/60-61){target="_quran"})

> The believers are those who, when God is mentioned, feel a tremor in
their hearts, and when His messages are recited to them they increase
them in faith, and in their Lord do they trust; who keep up prayer and
_spend out of what We have given them_. These, they are the believers
in truth. ([82:3-4](https://quran.com/82/3-4){target="_quran"})

Doctrine, ethics, and spirituality overlap to such a degree in the
Qur'an, that they are frequently interwoven in its definitions of
piety and belief.

> Piety is not that you turn your faces towards the East or West, but
pious is the one who has faith in God, and the Last Day, and the
angels, and the scripture, and the messengers, and gives away wealth
out of love for Him to the near of kin and the orphans and the needy
and the wayfarer and to those who ask and to set slaves free and keeps
up prayer and practices regular charity; and keep their promises when
they make a promise and are steadfast in [times of] calamity, hardship
and peril. These are they who are true [in
faith]. ([2:177](https://quran.com/2/177){target="_quran"})

> The sacrificial camels We have made for you as among the symbols of
God: in them is (much) good for you: then pronounce the name of God
over them as they line up (for sacrifice). When they are down on their
sides (after slaughter), eat thereof and feed such as those who live
in contentment and such as beg in humility: thus have We made animals
subject to you that you may be grateful. It is not their meat nor
their blood that reaches God: It is your piety that reaches
Him. ([22:36-37](https://quran.com/22/36-37){target="_quran"})

> Prosperous are the believers, who are humble in their prayers, and
who shun what is vain, and who are active in deeds of charity, and who
restrain their sexual passions -- except with those joined to them in
marriage, or whom their right hands possess, for such are free from
blame, but whoever seeks to go beyond that, such are transgressors --
those who faithfully observe their trusts and covenants, and who guard
their prayers. ([23:1-9](https://quran.com/23/1-9){target="_quran"})

The second passage refers to the day of sacrifice at the annual
pilgrimage to Makkah. The pilgrimage is one of Islam's five ritual
"pillars" and it is, even today, perhaps the most physically demanding
of all of them. It is stunning in its religious imagery, emotion, and
drama. And yet, here too, the Qur'an interconnects its social and
spiritual benefits.

Non-Muslims are often surprised by the spirit of optimism and
celebration that pervades Muslim rituals, especially during Ramadan
(the month of fasting) and the pilgrimage, which they assume are
performed mostly as atonement for past sins. Muslims, however,
perceive their rituals positively -- as spiritually and socially
progressive. They understand them to be a challenge and an
opportunity, as is life itself.

_Islam_ means "surrender" or "submission", a giving up of resistance,
an acquiescence to God's will, to His created order and to one's true
nature. It is a lifelong endeavor and trial, an endless road that
opens to boundless growth. It is a continuous pursuit that leads to
ever greater degrees of peace and bliss through nearness to God. It
engages all human faculties and its terms are unconditional. It seeks
a voluntary commitment of body and mind, heart and soul. Its
comprehensives may be brought to light by examining one of the great
questions of Christianity: "Is salvation obtained by faith or good
works?"

First, the question needs to be rephrased, because it is unnatural to
Muslims. Islam has known nothing similar to Christianity's
soteriology.  If a Muslim is asked: "How do you know you are saved?"
he or she will likely respond: "From what or from whom?" Earthly life
for Muslims is an opportunity, a challenge, a trial, not a punishment
from which one must be rescued. In the Qur'an, all creation, knowingly
or unknowingly, serves God's ultimate purposes. Thus it would not be
obvious to a Muslim that we needed to be saved from some entity. Even
Satan is stripped of his power in the Qur'an and reduced to the
function of eternal tempter, a catalyst for ethical decision making
and, hence, for moral and spiritual development. If anything, Muslims
feel that they may need to be saved from themselves, from their own
forgetfulness and unresponsiveness to God's many signs.

In a Muslim context, it would be more natural to ask, "How does one
achieve _success_ in this life: through faith or good works?" In
consideration of what we have already observed, the answer becomes
immediately obvious: both are essential. Otherwise, human existence
would not make sense and much of life would be superfluous. For the
Muslim, such a question would be analogous to asking, "What element in
water -- hydrogen or oxygen -- is necessary to quench one's thirst?"

Before considering what the Qur'an tells us about God, let us
recapitulate. The Qur'an claims that man's earthly life is not a
punishment and that it does not satisfy some whim of its
creator. Rather, it is a stage in God's creative plan. Mankind has
been endowed with a uniquely complex nature with contrary
inclinations. Through the use of his/her faculties (intellectual,
volitional, spiritual, moral, etc.) and the trials he or she is
guaranteed to face, an individual will either grow in his or her
relationship with God -- or as the Qur'an says "in nearness to God" --
or squander himself or herself in misdirected pursuits. The Qur'an
asserts that this earthly life will indeed produce a segment of
humanity that will experience and share in God's love; these are
called in the Qur'an _Muslimun_ (Muslims; literally, "those who
surrender"), for they strive to submit themselves -- heart, mind, body
and soul -- to this relationship. They are those who find peace,
security, and trust in God and who do good and strive to set things
right. To better understand how the lives we lead facilitate closer
communion with God, we turn now to God in the Qur'an.

## The Most Beautiful Names {-}

The Qur'an presents two obverse portraits of God and His activity.  On
the one hand, He is transcendent and unfathomable. He is "sublimely
exalted above anything that men may devise by way of definition"
([6:100](https://quran.com/6/100){target="_quran"}); "there is nothing
like unto Him" ([42:11](https://quran.com/42/11){target="_quran"});
and "nothing can be compared to Him"
([112:4](https://quran.com/112/4){target="_quran"}). These statements
warn of the limitations and pitfalls in using human language to
describe God, especially such expressions as are commonly used to
describe human nature and behavior, for man's tendency to literalize
religious symbolism often leads to the fabrication of misguiding
images of God. Nevertheless, the above statements serve only as
cautions in the Qur'an, since it too, of necessity, contains such
comparative descriptions. If we are to grow in intimacy with God, then
we need to know Him, however approximately, in order to relate to Him,
and toward this end speech is an obvious and indispensable tool.

Thus, in addition to declarations of God's complete incomparability,
we find His various attributes mentioned on almost every page. Often
used to punctuate passages, they occur typically in simple dual
attributive statements, such as, "God is the Forgiving, the
Compassionate" ([4:129](https://quran.com/4/129){target="_quran"}),
"He is the All-Mighty, the Compassionate"
([26:68](https://quran.com/26/68){target="_quran"}), "God is the
Hearing, the Seeing"
([17:1](https://quran.com/17/1){target="_quran"}). Collectively, the
Qur'an refers to these titles as _al asmaa al husnaa_, God's "most
beautiful names" ([7:180](https://quran.com/7/180){target="_quran"};
[17:110](https://quran.com/17/110){target="_quran"};
[20:8](https://quran.com/20/8){target="_quran"};
[59:24](https://quran.com/59/24){target="_quran"}).

> Say: Call upon God, or call upon the Merciful, by which ever you
call, to Him belong the most beautiful
names. ([17:110](https://quran.com/17/110){target="_quran"})

> God! There is no God but He. To Him belong the most beautiful
names. ([20:8](https://quran.com/20/8){target="_quran"})

> He is God, other than whom there is no other god. He knows the
unseen and the seen. He is the Merciful, The Compassionate. He is God,
other than whom there is no other God; the Sovereign, the Holy One,
the Source of Peace, the Keeper of Faith, the Guardian, the Exalted in
Might, the Irresistible, the Supreme. Glory to God! Above what they
ascribe to Him. He is God, the Creator, the Evolver, the Fashioner. To
Him belong the most beautiful names. Whatever is in the heavens and on
earth glorifies Him and He is exalted in Might, the
Wise. ([59:23-24](https://quran.com/59/23-24){target="_quran"})

The Divine Names are a ubiquitous element of Muslim daily life. They
are invoked at both the inception and completion of even the most
common tasks, appear in persons' names in the form of _Servant of the
Merciful, Servant of the Forgiving, Servant of the Loving_, etc.,
cried out in moments of great joy and sorrow, murmured repeatedly at
the completion of ritual prayers, and chanted rhythmically in unison
on various occasions. Because Muslims insert them into conversations
so frequently and effortlessly, some outsiders have accused Muslims of
empty formalism. But this reflects a lack of understanding, for the
truth is that the Divine Names play such an integral role in the lives
of the faithful that their use is entirely natural and uninhibited.

The Divine Names are, for Muslims, a means of turning towards God's
infinite radiance. Through their recollection, believers attempt to
unveil and reorient their souls towards the ultimate source of all. A
knowledge of them is essential if one is to comprehend the
relationship between God and man as conceived in the Qur'an and as
experienced by Muslims.

In his _Concordance of the Qur'an_ [^17], Kassis renders in English
most of the titles and adjectives applied to God in the scripture,
together with some of their different shades of meaning. His list is
not exhaustive. Other Qur'an scholars have obtained longer lists and
other possible meanings in English could have been added, as it is
hard to do justice to the Arabic original. For example, _rabb_ which
Kassis renders as "Lord", conveys the idea of "fostering",
"sustaining", and "nourishing". According to the famous scholar of
Arabic, al Raghib al Isfahani, it signifies "the fostering of a thing
in such a manner as to make it attain one condition after another
until it reaches its goal of completion" [^18]. Clearly, the word
"Lord" does not bring out this idea. The following list of divine
attributes is taken from Kassis.

[^17]: Hanna E. Kassis, _A Concordance of the Qur'an_ (University of
    California Press, 1983).

[^18]: Quoted by Ali, _The Holy Qur'an,Text, English Translation and
    Commentary_.

**Divine Names and Attributes:**

**Able** (_qadir_); **Absolute** (_samad_); **One Who Answers**
(_ajaaba_); **Aware** (_khabeer_); **Beneficent** (_rahmaan_);
**Benign** (_barr_); **Bestower** (_wahhaab_); **Blameless**
(_haasha_); **Bountiful** (_akrama, tawl_); **Clement** (_'afoow,
haleem, ra'oof_); **Compassionate** (_raheem_); **Compeller**
(_jabbaar_); **Creator** (_badee', bara'a, fatara, khalaqa, khallaq_);
**Deliverer** (_fattaah_); **Disposer** (_wakeel_); **Embracing**
(_wasi'a_); **Eternal** (_qayyoom, samad_); **Everlasting**
(_qayyoom_); **Everlasting Refuge** (_samad_); **Evident**
(_dhahara_); **Exalted** (_ta'aala_); **the Exalter** (_rafee'_);
**Faithful** (_aamana_); **Fashioner** (_sawwara_); **First**
(_awwal_); **Forgiver** (_ghaffaar, ghafuur_); **Gatherer**
(_jama'a_); **Generous** (_kareem_); **Gentle** (_lateef, ra'oof_);
**Giver** (_wahhaab_); **Glorious** (_'adheem, akrama, majeed_);
**God** (_Allah, ilaah_); **Gracious** (_lateef, rahmaan_);
**Grateful** (_shakara_); **Great** (_kabeer_); **Guardian**
(_hafeedh, wakeel, waleey_); **Guide** (_had'a_); **He** (_huwa_);
**Hearing** (_samee'_); **High** (_'aleey_); **Holy** (_quddoos_);
**Honorable** (_akrama_); **Informed** (_khabeer_); **Inheritor**
(_waritha_); **Inward** (_batana_); **Irresistible** (_jabbaar_);
**Most Just Judge** (_hakama_); **Kind** (_lateef, ra'oof_); **King**
(_malik, maleek_); **Knower** (_'aleem, 'alima, khabeer_); **Last**
(_aakhir_); **Laudable** (_hameed_); **Light** (_nuur_); **Living**
(_hayy_); **Lord** (_rabb_); **Loving** (_wadood_); **Majestic**
(_jalaal, takabbara_); **Master of the Kingdom** (_malaka_);
**Merciful** (_rahmaan_); **Mighty** (_'azeez, 'adheem_);
**Omnipotent** (_iqtadara, qadeer, qahara, qahhaar_); **One** (_ahad,
waahid_); **Originator** (_fatara_); **Outward** (_dhahara_);
**Overseer** (_aqaata_); **Pardoner** (_'afoow_); **Peaceable**
(_salaam_); **Powerful** (_qadira, qadeer, aqaata_); **Praiseworthy**
(_hameed_); **Preserver** (_haymana_); **Protector** (_mawl'a, wala',
waleey_); **Provider.** (_razzaaq_); **Quickener** (_ahyaa_);
**Reckoner** (_haseeb_); **Sagacious** (_khabeer_); **Seeing**
(_baseer_); **Shaper** (_sawwara_); **Splendid** (_akrama_);
**Strong** (_qaweey_); **Sublime** (_takabbara_); **Subtle**
(_lateef_); **Sufficient** (_ghaneey, istaghn'a, kafa_); **Superb**
(_takabbara_); **Sure** (_mateen_); **Tender** (_lateef_);
**Thankful** (_shakara, shakoor_); **True** (_haqq_); **Trustee**
(_wakeel_), **one who Turns towards others** (_tawwaab_); **Watcher**
(_raqeeb_); **Wise** (_hakeem_); **Witness** (_shaheed_).

There is great variance in the number of times these attributes are
applied to God in the Qur'an. For example: God is called _Allah_ (the
God) approximately 2698 times, _Rabb_ (Lord, the Sustainer) almost 900
times, _al Rahmaan_ (the Merciful) 170 times, _al Raheem_ (the
Compassionate) 227 times, _al Ghaffaar_ (the Forgiving) and _al
Ghaffoor_ (the Forgiving) a total of 97 times, and _al Lateef_ (the
Kind, the Gentle) 7 times. This repetition and variation have an
important influence on Muslim religiosity. To appreciate this, one
first needs to realize just how often a believer is in contact with
the Qur'an.

A practicing Muslim will recite the Qur'an at the very least five
times per day during his/her obligatory prayers. Many Muslims listen
to the Qur'an on cassette tapes similar to the way westerners listen
to music, very many read some portion of it daily for guidance and
study, and a significant number have memorized it in its entirety. As
they continue through the Qur'an, Muslims are constantly recalling the
Divine Names and Attributes, which appear again and again on virtually
every page. Through this continuous recollection, a certain spiritual
vision or image of God writes itself on the Muslim heart and mind,
with the more frequently mentioned attributes attaining a certain
position of priority over those less frequently stated.  If we were to
attempt to visualize this effect, we might picture a pyramid of the
Most Beautiful Names: _Allah_ would be at the apex and then the
attribute of _Rabb_ (Nourisher, Sustainer, Lord) connected to and
proceeding from Allah somewhat below; then the attributes of Mercy,
_al Rahmaan_ and _al Raheem_, further on down, proceeding from and
manifesting the attributes above them, like rays of light flowing from
a lamp; then Forgiveness, _al Ghafaar_ and _al Ghafoor_, and Creator,
_al Khallaaq_, proceeding from Mercy; and so on (see the diagram
below).

![](img/diagram1.png)

In this way, a Muslim develops a completely immaterial conception of
God; he or she approaches God through mind, heart, soul, feelings, and
intuition, not through physical imagery. This, I feel, is the main
source of Islam's famed iconoclasm. It is not a harsh fanaticism that
has its roots in a culturally and artistically primitive desert
community; it is a corollary to the way Muslims conceive of and relate
to God through concepts that express intrinsic qualities and
activities rather than through visual images. Thus we have the unique
Muslim religious art of calligraphy, which does not consist of
portraits and statues, but of words, often the Divine Names and
Qur'anic verses, beautifully and elegantly written and producing
wondrous and intricate symmetries and designs that must be studied
very carefully to decipher the hidden meanings and discover the truth
behind the beauty.

Another look at the Divine Names reveals a perplexity. Since the names
_al Rahmaan_ (the Merciful), _al Raheem_ (the Compassionate), _al
Rabb_ (the Sustainer, Nourisher), _al Mawlalal Waleey_ (the Protector,
Guardian), and _al Ghaffaar / al Ghafoor_ (the Forgiving) appear so
frequently, we would expect that the seemingly closely related name,
_al Wadood_ (the Loving), would appear more than twice. If we include
the large number of references to God's loving or withdrawing His love
from others, then we might feel it appropriate to increase this
total. But on close examination of such instances, we notice that
God's love in the Qur'an is not universal, and this is in fact what
sets this attribute apart from the others just mentioned. His mercy,
for example, embraces all creation and includes even the worst sinners
([7:156](https://quran.com/7/156){target="_quran"};
[30:33](https://quran.com/30/33){target="_quran"};
[30:36](https://quran.com/30/36){target="_quran"};
[30:46](https://quran.com/30/46){target="_quran"};
[40:7](https://quran.com/40/7){target="_quran"};
[42:28](https://quran.com/42/28){target="_quran"}), and His sustaining
and nourishing extend to all. God is the only true protector of every
soul ([2:107](https://quran.com/2/107){target="_quran"};
[6:62](https://quran.com/6/62){target="_quran"};
[10:30](https://quran.com/10/30){target="_quran"}) and He immediately
accepts all sincere repentance
([4:110](https://quran.com/4/110){target="_quran"};
[13:6](https://quran.com/13/6){target="_quran"};
[39:53](https://quran.com/39/53){target="_quran"}). But when the
Qur'an speaks of God's love, it is pointing to a very special
_relationship_, willfully entered into by God and man, a relationship
that the Qur'an says most of humanity will reject
([17:89](https://quran.com/17/89){target="_quran"};
[25:50](https://quran.com/25/50){target="_quran"};
[27:73](https://quran.com/27/73){target="_quran"}). Although God's
mercy, compassion, and care shine on all mankind, only those who turn
to Him and strive throughout their lives to surrender themselves to
Him will attain a bond of love with Him.  This love signifies a love
shared -- a love received and given -- and a mutual involvement. Since
relatively few will choose to enter into it, we should probably not be
at all surprised at the limited use of the name _al Wadood_ in the
Qur'an, for while a loving relationship with God is available to
everyone, most will not enter into one.

Let us turn again to the Divine Names, this time recalling our
discussion of what Islam requires of man. As we make our way through
the Qur'an, we are reminded persistently of the attributes of God and
the qualities that we are supposed to cultivate in ourselves. It is
not long before it dawns on us that there is considerable intersection
between the two, for almost every virtue that we are to develop in
ourselves through our actions toward others has its origin and
perfection in God. For example, we are to grow in beneficence,
benignity, bountifulness, clemency, compassion, faithfulness, our
willingness to forgive, generosity, gentleness, givingness,
graciousness, honorableness, justice, kindness, knowledge, love of
others, mercy, peacefulness, protectiveness of the weak, truthfulness,
trustworthiness, and wisdom. Yet, these originate in God as His
attributes of perfection. Thus, by developing these attributes in
ourselves, we are actually growing _nearer_ -- to use a Qur'anic term
-- to their infinite source. Hence, the more we come to possess these
qualities, the more we may come to know God. Since it is possible for
human beings to experience and acquire these virtues at higher levels
than other creatures, they have the potential to relate to God in a
uniquely intimate way.

An analogy may help elucidate this point. Assume that I have a pet
gold fish, a dog, and three daughters. The gold fish, being the most
limited in terms of intellect and growth, could only know and
experience my love and compassion at a relatively low level no matter
how much kindness I direct toward it. The dog, who is a more complex
and intelligent animal than my fish, can feel warmth and affection for
another on a much higher level and therefore can experience the love
and compassion I shower on him to a much greater degree. Yet my
daughters, as they mature, have the potential to feel the intensity of
my love and caring for them on a plane that my dog could never
conceive of, because they have the capacity to know first hand,
through their own emotions and relationships, deeper and richer
feelings than my dog. And I would have to say that the love I now have
for my parents is greater than the love I had for them when I was a
child, because by having children of my own, I came to better know and
understand the power of the love my mother and father gave to me.

Pushing the above analogy a bit further, we see that it is not enough
for my daughters to invest themselves only in other human
relationships, as they will never experience the intensity of my
feelings toward them unless they also acknowledge and turn to me as
their father, that is, unless they accept and enter into that
parent--child relationship. I could have all the fatherly feelings for
them in the world, but if by some strange set of circumstances they
totally rejected or were oblivious of them, then they would never
enter into a relationship of love with me. All the caring and kindness
I had for them would be of little benefit to them. This, I believe, is
why the Qur'an insists on both faith in God and good works toward our
fellow man, because both are necessary if we are to come to know
God. If an atheist is a great humanitarian, he may gain the love and
admiration of neighbors and friends and perhaps great
self-satisfaction and meaning in life, but he will still be
spiritually empty. I am not insisting that such a person would be
destined for eternal suffering, for that would depend on factors that
are impossible for us to know or measure, such as his personal
limitations, the environment in which he lived, the opportunities that
were presented to him, and many others. However, the purpose of the
Qur'an is not to discuss such precarious, borderline cases as possible
options; it guides toward what will most benefit man and warns of what
will destroy him.

"Every soul must taste of death; then to Us you will be returned"
([29:57](https://quran.com/29/57){target="_quran"}). This truth
reverberates throughout the Qur'an. It reminds the reader that the end
and purpose of all earthly endeavor is this reunion.  Ultimately, it
is our relationship with God that matters. However, it would be wrong
to say that it is all that matters, for, as we discovered, our
relationship with God is bound intimately to our responses to our
fellow man. Rituals, inspiration, contemplation, and remembrance
([3:191](https://quran.com/3/191){target="_quran"};
[4:103](https://quran.com/4/103){target="_quran"}) all play integral
parts in bringing us nearer to God, but so does our growth in
virtue. The more a believer grows in the attributes that originate in
God, the closer and more intense is his/her bond with Him and the
greater is his/her capacity to receive and experience His infinite
beauty and power, both in this life, and incomparably more so in the
next life, where earthly distractions and masks are removed.

This is much more than having our own personal experiences of goodness
to approximate God's transcendent goodness; it involves a kind of
intimacy and knowing that cannot even be shared by two human
beings. There is the well-known expression, "In order to begin to
understand me, you have to walk a mile in my shoes." This means that
we cannot truly know another person unless we could somehow fully
enter into his life and experience it from his personal
perspective. As this is not really possible -- since we are always, in
a sense, outside that experience -- the implication is that we are
very limited in our ability to sympathize with others. Certainly, we
can never fully know God, but yet we can experience His being in a
uniquely profound way.

The Prophet once told his companions that one percent of the mercy
that God bestows on creation is manifested in human behavior. [^19]
The saying is meant to impress upon us the greatness of God's
mercy. However, it also indicates that the mercy we demonstrate and
feel is but an infinitesimal fragment of God's limitless mercy. Thus,
God grants us the ability to participate in and experience His mercy
firsthand in our earthly lives, not only as recipients, but as givers
of mercy as well, for when we are merciful toward another creature,
that being receives something of God's mercy through us.

[^19]: From _Sahih al Bukhari_ and _Sahih Muslim_ as translated in
_Riyad as Salihin_ of Imam Nawawi, 94.

The same holds for almost all of the other Most Beautiful Names of
God: if we are truthful, we are experiencing a fraction of the truth
that comes from God; if we attain wisdom, all wisdom flows from God;
if we obtain power, there is no power but in God. A mother
participates in creation on a level that will always be a mystery to
men, and hence her experience of the attribute of Creator is
especially profound. Perhaps the same can be said of her experience of
the Merciful and the Compassionate. Many ancient Muslim scholars felt
that the female is more sensitive to certain divine attributes than
the male and conversely. Such names as the Powerful, the Protector,
the Provider, were believed to be more suited to the male.

The Qur'an informs us that God breathes something of His spirit into
every human soul
([32:9](https://quran.com/32/9){target="_quran"}). This seems to
indicate that every human has a seed of the Divine Attributes within
him/her or, in other words, that the virtues he/she experiences are
but a breath of the Divine Names. The concept, that the more one
pursues virtue the greater becomes his or her capacity to experience
the divine, is brought to light by the saying of Muhammad that asserts
that the more a believer persists in worship and doing good, the
clearer his heart becomes so that it is better able to receive the
divine light, and that if someone is negligent in these, his heart
becomes rusted and incapable of receiving divine illumination. [^20]
The goal of the devout Muslim is to grow continually as a receiver and
transmitter of God's infmite radiance, to be drawn ever nearer to the
source of all that is beautiful, to accept the office of delegate of
the Possessor of the Most Beautiful Names, and, hence, to serve as a
_khalifah_ (vicar) of God on earth as described in
[2:30](https://quran.com/2/30){target="_quran"}.

[^20]: From Ibn Majah and Tirmidhi as translated in Mazhar U. Kazi,
_Guidance from the Messenger (Jamaica, NY: ICNA Publications, 1990),
84.

It appears that we have returned to where we first started. This
entire discussion of the purpose of life in the Qur'an was launched by
the story of man as told in verses thirty through thirty-nine of the
second surah. One might assume that it began with the angels' question
and, hence, with a doubt or criticism, but this would be an
oversight. In fact, our investigation was inspired by an astounding
affirmation that was so positive and optimistic that the angels'
question had to be raised: "Behold, your Lord said to the angels: 'I
am going to place a vicegerent on earth.'"

How can man, this most destructive and corruptible creature, serve as
a deputy and representative of God? Of all beings, how can man act as
an agent and emissary of his Lord? Human history and human nature seem
to be at odds with this election. But when we look at mankind from
this viewpoint, we, like the angels, are seeing only one side of
reality and are neglecting man's potential for goodness and
self-sacrifice as well as his ability to strive to live by the highest
standards of virtue. Every person has it within himself to receive,
represent, and impart to others the mercy, compassion, justice, truth,
bounty, and love that originate in God and thus to act in this way as
His emissary on earth. The Qur'an maintains that human nature contains
this inclination and potential
([30:30](https://quran.com/30/30){target="_quran"}), but it requires a
choice that must be faced continually and remade. The office of
_khalifah_ is not simply conferred; it must be accepted voluntarily
and it must be a lifelong commitment. The Qur'an does not ask for
human perfection, but rather asks that we persevere in striving for
self-improvement and that we never become complacent
([2:197](https://quran.com/2/197){target="_quran"};
[5:2](https://quran.com/5/2){target="_quran"};
[7:26](https://quran.com/7/26){target="_quran"}) or despondent
([15:56](https://quran.com/15/56){target="_quran"};
[39:53](https://quran.com/39/53){target="_quran"};
[15:55](https://quran.com/15/55){target="_quran"}) about our progress.

## First Objection {-}

It may be that the Qur'an's concept of life has a certain appeal and
coherence. However, let us not allow ourselves to be romanced into
accepting it. The idea that growth in virtue leads to inner peace and
well-being, and that it contributes an abiding beauty to life, is easy
to admit. The notion that it also allows us to receive and experience
God's infinite attributes to ever greater degrees is sensible. Yet, is
there not an obvious and glaring problem with this conception? Cannot
this divine plan be charged with gross inefficiency? Why did not God
simply create us with these virtues from the start? Why did He not
program mercy, truthfulness, compassion, kindness, and the rest into
us and bypass this earthly stage in our existence? Thus, we never
really got beyond the angels' question: Why not make man someone
greater than he is -- someone like the angels?

To answer this, we need not search far, but only have to look within
our own selves. If we know nothing else of the virtues under
consideration, we certainly know they cannot exist in a being at very
high levels if they were merely programmed into it. Virtue, if
programmed, is not true virtue as we conceive of it, but something
less. We can program a computer to be always correct, but we could not
describe it as a truthful computer. We do not consider a stethoscope
to be merciful although it aids the sick. The Qur'an presents angels
as creatures without free will, but man can rise to heights much
higher or sink to depths much lower than them.

Virtues are abstract concepts and difficult to define, but I believe
that we can agree that to grow in virtue at least three things are
needed: _Free will_, or the ability to choose; _intellect_, so that
one is able to weigh the consequences of his or her choices and learn
from them; and third, and equally important, _suffering and
hardship_. As we saw, the Qur'an emphasizes strongly all three of
these while discussing man's spiritual evolution. To grow in
compassion, for example, is inconceivable without suffering. It also
requires choice, the ability to choose to reach out to someone in need
or to ignore him. Intellect is necessary so that one can estimate how
much of oneself will be invested in showing compassion to the
sufferer.  Similarly, to be truthful involves a choice not to lie and
is heightened when telling the truth may lead to personal loss and
suffering, which can be predicted through the use of one's reason.

How often do we hear in plays, movies, and songs statements like, "You
never really loved me, because when I was down and out and my life was
falling apart you only thought of yourself and you left me!" Such a
statement acknowledges the essential roles of hardship, choice, and
intellect in love. The same could be said of the famous wedding vow
that asks two people if they are ready to commit themselves to each
other "for richer or poorer, in sickness and in health, until death".
It is the same with all the virtues: these three elements are crucial
to our growth in them.

_When my oldest daughter, Jameelah, was a baby, she became very ill
one night. The only way I could get her to sleep was to carry her on
my shoulder while I paced the apartment and sang softly to her. If I
stopped singing or tried to put her down, she would immediately wake
up crying. So all night long, about eight straight hours, I carried my
little girl and kept on singing. Her fever broke around dawn and she
was then able to sleep on her own. Of course, by this time I was
exhausted. My back was killing me, my throat was hoarse, and I had to
be at work in an hour._

_When I recently recalled this episode for her, lameelah asked me,
"Weren't you mad at me, daddy?"_

_I was surprised by her question, because the feeling never at any
moment that night entered my heart._

_"Mad at you!" I told her: "Sweetheart, I couldn't have loved you
more!  It is a memory that I will always cherish."_

Love, compassion, and caring are born of such experiences. They have a
beauty and value that is, to use an expression of the Prophet, "worth
more than this world and all it contains"[^21]. They also frequently
contain opportunities for healing previous wounds and recovering past
losses.

[^21]: An expression used frequently by Prophet Muhammad. See _Riyad
as Salihin_ of Imam Nawawi, 221-22.

_Sarah, my second daughter, broke her leg when she was only a year
old, and I had to stay that night with her in the hospital. She came
so quickly after Jameelah -- only a year separates them -- and my work
load at the university had increased so much, that I had spent hardly
any time with Sarah until then. When Jameelah was a baby we were
inseparable, but now a year had past in Sarah's life and I hardly knew
her._

_Sarah gripped my hand through the night; if I let go of it she would
scream. The muscle spasms in her leg were keeping her awake. I had to
lean over her bed rail to reach her hand and the rail was digging into
my side. It was another sleepless, uncomfortable night._

_As I watched this little child fight and struggle through the hours
with her pain, I discovered Sarah for the first time. As I gazed into
her big brown eyes and studied her expressions and reactions to my
touch and my voice, I found how much this child and I were alike --
how similar our personalities were. I was so ashamed that I had not
taken the time before this to get to know her and I realized how much
I had lost and how much we both needed each other. I vowed that
evening to begin to work on my relationships with all my children and
not to wait for crises to bring us together._

These incidents were two minor trials in my life from which I learned
so much. I wonder how much greater must be the experience of
motherhood, for if I could gain so much benefit from these two
moments, how much greater must be the potential personal gain from
carrying a child for nine months? Perhaps this is why the Prophet
Muhammad told his companions that motherhood puts paradise at a
woman's feet.

It also helps me understand why Islam places so much emphasis on
family ties, since these relationships are for us the most intense and
demanding and thus provide some of the most important opportunities
for personal growth. The Prophet stated that "marriage is half of
faith", since men and women only attain the fullness of their
personalities as spouses and parents. Consider also the verse in the
Qur'an that states that marriage provides spouses with a special way
to experience love and mercy and that we should reflect on this most
important sign:

> And among His signs is this, that He created for you spouses from
among yourselves, that you may dwell in tranquillity with them, and he
has placed love and mercy between you. Truly in that are signs for
those who reflect. ([30:21](https://quran.com/30/21){target="_quran"})

As parents and as children, we may experience the widest range of the
Divine Names as givers and receivers, respectively. When we attain
middle age and are simultaneously parent and child, we reach a point
in our lives that allows us to learn of God through both
perspectives. The following two passages highlight this stage in our
lives.

> Your Lord has decreed that you worship none but Him, and that you be
kind to parents. Whether one or both of them attain old age in your
life, never say to them even "Uff!" (a minor expression of contempt)
nor repel them, but address them in terms of honor. And out of
kindness, lower to them the wing of humility and say, "My Lord! have
mercy on them even as they cherished me when I was a little one."
([17:23-24](https://quran.com/17/23-24){target="_quran"})

> We have enjoined on mankind kindness towards parents. In pain did
his mother bear him and in pain did she give him birth. The carrying
of the [child] to his weaning is thirty months. At length when he is
fully grown and attains forty years he says, "O my Lord! Grant me that
I may be grateful for the favor which You have bestowed upon me, and
upon both my parents, and that I may work righteousness such as You
may approve; and be gracious to me in my children. Truly I have turned
to You and truly I do bow in
Islam. ([46:15](https://quran.com/46/15){target="_quran"})

The first of the above passages links worship of God with kindness
toward parents. It recalls their care and tenderness, their
self-sacrifice is clearly implied, and thus they deserve our utmost
respect. The second passage shows that honoring one's parents is an
expression of gratitude toward God. It especially honors the mother's
role because of the greater degree of pain and suffering she goes
through in raising children.

Islam sees a unity and harmony between body, mind and soul, and
unifying principles that govern all three. Coaches often tell athletes
"no pain, no gain", by which they mean that in order to develop our
physical strengths, we must be willing to suffer. Teachers inform
students that they must work hard in order to grow
intellectually. Muslims understand that the same law applies to
spiritual development. Moral and spiritual growth requires the
discipline of one's will, the use and development of one's mind, and
the experience of hardship and suffering. The last of these should
never discourage the Muslim, for the Prophet told his followers that a
believer should always thank God in good times and in hard times,
because he or she can benefit from both. [^22]

[^22]: From _Sahih Muslim_ as translated in Kazi, _Guidance from the
    Messenger_, 151-52.

## Second Objection {-}

Let us agree that virtues, like love, compassion, truthfulness, and
kindness, are not programmable and, furthermore, that in order to grow
in these qualities we must possess the ability to choose and to reason
and that we need to face adversity. Is there not then an obvious
counter-example? Does or did God grow in these qualities? Does He
weigh and choose between alternatives? Does He need to reason things
out? Did He have to learn these attributes? Can He experience
suffering?

This objection confuses creator with creature and bestower with
receiver. God, the Creator, is described as eternal, absolute,
perfect, transcending time and space. He is not diminished nor
improved by His activity. He is the continuous source and preserver of
all existence. His Most Beautiful Names indicate that He is the
perfect and only real source of the attributes in which we must
increase our capacity to receive and experience. He is the absolute
source of all the mercy, compassion, wisdom, truth, etc., that flows
through creation.

Man as a creature, by definition, becomes. He experiences growth and
decline. Creation, according to the Qur'an, passes from one state into
another ([13:5](https://quran.com/13/5){target="_quran"};
[28:88](https://quran.com/28/88){target="_quran"};
[32:10](https://quran.com/32/10){target="_quran"}). Hence man, in
particular, is a changeable being.  The fact that God is not is a
Qur'anic axiom ([33:62](https://quran.com/33/62){target="_quran"};
[35:63](https://quran.com/35/63){target="_quran"}). God is the sole
origin of -- the explanation behind -- all virtue, as His being
accounts for its very existence. He does not grow in His ability to
experience mercy; He provides the mercy in which we share. He does not
increase in wisdom; He guides us to the wisdom that originates in
Him. He does not develop His power; He empowers others. Far from
providing a counter-argument, His being accounts for the attributes
that we are to pursue.

## To Live and Learn {-}

We have seen how the Qur'an underscores the learning ability of man
and the instructional value of life in human moral and spiritual
growth. That man will err along the way in his choices is inevitable,
but God, in the Qur'an, does not expect us to be infallible; instead,
He has granted us the ability to learn and gain from our mistakes. The
Qur'an warns of the dangers of sin but also explains that if someone
realizes his errors, repents, and believes and does good thereafter,
God transforms his once-destructive deeds into beneficial ones.

> He who repents and believes and does good; for such God changes
their evil deeds to good ones. And God is ever Forgiving and
Merciful. ([25:70](https://quran.com/25/70){target="_quran"})

This verse seems to include the notion of the positive value of our
acknowledged and amended errors. It is certainly good to avoid an evil
solely out of obedience, but if one has also experienced personally
its destructive and painful consequences, the wisdom and benefit
behind its avoidance become inculcated in his heart and mind. It is
like the child who avoids the kitchen stove after having been burned
by it: he is no longer simply obeying his parents but is avoiding what
he knows intrinsically to be harmful.

The Qur'an describes how even God's elect grew from past errors:
Abraham discovers monotheism through a sequence of mistaken attempts
([6:75-82](https://quran.com/6/75-82){target="_quran"}); Moses commits
involuntary manslaughter but repents and learns from it
([28:15-19](https://quran.com/28/15-19){target="_quran"}); and David
is taught an important lesson that helps him realize a past wrong
([38:21-26](https://quran.com/38/21-26){target="_quran"}). The
Qur'an's several criticisms of Prophet Muhammad are clearly meant to
instruct him and his community.

God does not require us to become perfect before admitting us into His
grace. Rather, the more we grow in goodness and in faith, the more we
avail ourselves of it -- the more our hearts become opened to
receiving it. This is the lifelong effort of the believer: to refine
his spirituality and thus to enter into an ever more intimate
relationship with God. As long as we are alive, our personal growth
has no reachable upper limit, for we can never reach a stage at which
we can no longer gain from doing good. A Muslim believes that no good
deed is superfluous. No matter how small it is, it will benefit him in
this life and the next.

> Then anyone who has done a speck of good, will see it. And anyone
who has done a speck of evil will see
it. ([99:7-8](https://quran.com/99/7-8){target="_quran"})

In the Qur'an, God does not seek to bar men and women from His grace;
He desires to guide them to it and He pursues them incessantly and
aggressively. He invites, reminds, challenges, argues, shames,
entices, and threatens. He inflicts calamities upon the sinful so that
"perhaps they will return [to God]"
([7:168](https://quran.com/7/168){target="_quran"};
[30:41](https://quran.com/30/41){target="_quran"};
[32:21](https://quran.com/32/21){target="_quran"},
[43:28](https://quran.com/43/28){target="_quran"}).

To those won over to its calling, the Qur'an provides much of what may
be termed practical spiritual advice. First of all, one should avoid
the heinous sins (murder, adultery, cheating the poor, etc.). In
conjunction with faith in God, this will insure paradise in the next
life.

> If you avoid the great sins of the things which are forbidden you,
We shall expel out of you all the evil in you, and admit you to the
gate of great honor. ([4:31](https://quran.com/4/31){target="_quran"})

> That which is with God is better and more lasting for those who
believe and put their trust in their Lord; those who avoid the great
sins and indecencies and when they are angry even then
forgive. ([42:36-37](https://quran.com/42/36-37){target="_quran"})

> Those who avoid great sins and indecencies, only [falling into]
small faults -- truly your Lord is ample in
forgiveness. ([53:32](https://quran.com/53/32){target="_quran"})

After repentance, one should try to make amends in order to guard
against a spiritual decline. In the same vein, the Qur'an states that
good deeds offset evil ones
([11:114](https://quran.com/11/114){target="_quran"};
[13:22](https://quran.com/13/22){target="_quran"};
[28:54](https://quran.com/28/54){target="_quran"}). The idea here
seems to be that if we take a step backward, we should try to counter
it immediately by taking several steps forward so as not to lose
progress. A key related eschatological symbol is that of the Balance
on the Day of Judgment, which will weigh a person's good deeds against
his evil ones. If the former outweigh the latter -- if the individual
is essentially a good person -- then he or she will enter eternal
bliss. We should keep in mind, however, that God has made it so that
the positive rewards of righteousness far outweigh the negative
consequences of wrongdoing
([28:84](https://quran.com/28/84){target="_quran"};
[40:40](https://quran.com/40/40){target="_quran"}).

All this may seem too empirical, since it is impossible for us to
detect precisely and accurately measure our good and sinful
doings. However, the Qur'an is not providing an exact science of
spiritual growth, but rather a helpful conceptual model. The Muslim is
the first to admit his utter dependence on and trust in God's mercy
and kindness, for he knows that he is on earth for a purpose. This
conceptualization helps him to pursue it, even if it is not entirely
clear to him what life's purpose is or has never agonized over its
meaning.

![](img/diagram2.png)

Muslims say that if your faith is not increasing, then it is about to
decrease or else it already has. If we were able to plot a person's
spiritual growth against time, a Muslim would envision it as a
continuous curve that, at any point, is either ascending, descending
or at a critical turning point.  According to this perspective, faith
is not a steady state. A believer must be on guard against unwittingly
slipping into a downward slope, and so he must always review the
current state of his religiosity.

"How is your faith?" One Muslim will ask another. There is no precise
measure, but there are a number of diagnostic checks that may help:
"Do I feel closer to or farther from God in my five daily prayers
lately?"; "Am I giving more or less in charity these days?"; "Was I at
greater or lesser peace with myself and with others in the past?" With
such selfanalysis a Muslim hopes to stay on what the Qur'an describes
as "the uphill climb"
([90:11](https://quran.com/90/11){target="_quran"}).

> Surely he thought that he would never return (to God). But surely
his Lord is ever Seer of him. But no, I call to witness the sunset
redness, and the night and that which it drives on, and the moon when
it grows full, you will certainly travel stage after stage. But what
is the matter with them that they believe not?
([84:14-20](https://quran.com/84/14-20){target="_quran"})

## Trial and Error {-}

Life is often described as a great classroom -- the ultimate learning
environment. This description accords well with the Qur'an. As the
best of all teachers, God not only provides us with the essential
tools for learning but guides us to learn and grow through personal
discovery as well. Thus the Qur'an states that God "taught [mankind]
by the pen -- taught man that which he did not know"
([96:4-5](https://quran.com/96/4-5){target="_quran"}), even though
man's acquisition of reading and writing skills was a slow and gradual
human development, one in which God's influence is easily missed. So
subtle and effective is God's teaching that man often attributes his
intellectual achievements entirely to himself. So the Qur'an
continues:

> No, but man is overbold, in that he views himself as
> independent. ([96:6-7](https://quran.com/96/6-7){target="_quran"})

Earthly life provides us with a sense -- a false one actually -- of
independence of and distance from God, a sense that drives us to learn
and apply what we learn seemingly on our own. This is somewhat like
when a teacher leaves the classroom and then watches through a one-way
mirror to see how his students interact when faced with solving
problems: no longer able to appeal to their teacher, the pupils are
forced to solve the problems independently, while all the while the
teacher monitors their progress and intercedes only when he deems it
necessary. This is an extremely effective teaching device, for there
is no substitute for firsthand experience.

In such a setting, one of the principle ways in which we learn is
through trial and error. When I speak of trial and error here, I am
referring not only to tests and mistakes we meet with on the
intellectual plane, but also on the moral and spiritual plane,
although the two overlap and are complementary. When a mistake we make
has moral implications, it becomes a sin, the gravity and harm of
which increases with our awareness of its wrongfulness
([4:17-18](https://quran.com/4/17-18){target="_quran"}). But if we
repent and avoid it thereafter, it could, as we have seen, assist our
spiritual growth. Thus we can learn and grow from our
mistakes. Without the potential for error, realization, and reform,
our spirituality would stagnate. So vital are these to our development
in this earthly stage that the Prophet Muhammad reported that if
mankind ceased sinning, God would replace them with other creatures
who would continue to sin and repent and gain God's forgiveness.[^23]

[^23]: Ibid., 95.

Earthly trial is another key ingredient of the divine scheme: "We will
try you with something of fear"
([2:155](https://quran.com/2/155){target="_quran"}), "God tests what
is in your hearts"
([3:154](https://quran.com/3/154){target="_quran"}), "You shall surely
be tried in your possessions"
([3:186](https://quran.com/3/186){target="_quran"}), "He tests you in
what He has given you: so strive as in a race in all virtues"
([5:51](https://quran.com/5/51){target="_quran"}), "0 believers, God
will surely test you"
([5:97](https://quran.com/5/97){target="_quran"}), "We try you with
evil and good" ([21:35](https://quran.com/21/35){target="_quran"}),
"He may try some of you by means of others"
([47:4](https://quran.com/47/4){target="_quran"}). The Qur'an states
that God created the entire cosmos in order to test mankind
([11:7](https://quran.com/11/7){target="_quran"}) and that God created
death and life on earth to the same end
([67:2](https://quran.com/67/2){target="_quran"}). Since these tests
can not improve God, they must be for our intellectual and spiritual
refinement, and therefore the universe and our existence in it are
designed to produce this testing and learning opportunity.

The Day of Judgment ([1:4](https://quran.com/1/4){target="_quran"}) is
depicted as the moment when the results of our efforts are
realized. The Qur'an's depiction of it has an unmistakable academic
tone. It resembles the end of term or graduation day on a college
campus. Mankind will be sorted out into three classes
([56:7-56](https://quran.com/56/7-56){target="_quran"}): The Foremost
in Faith, those who excelled in their submission to God and are
brought nearest to Him; the Companions of the Right Hand, those who
did well enough on earth to enter paradise but who do not reach the
level of excellence of the Foremost in Faith; and the Companions of
the Left Hand, those who failed in life and who face suffering in the
life to come. The record of all deeds, however small or great, will be
brought out. The sinful will be in great terror this moment as they
sense their fate
([18:49](https://quran.com/18/49){target="_quran"}). The faces of
those who failed in life will be humiliated, laboring, and weary,
while those who were successful will have joyful, exuberant faces
([88:1-16](https://quran.com/88/1-16){target="_quran"}). The
successful will receive their earthly records in their right hands and
will gleefully show them to others; the failures, consumed by grief
and embarrassment, will be given their records in their left hands or
will hold them behind their backs
([69:19-30](https://quran.com/69/19-30){target="_quran"};
[9:10](https://quran.com/9/10){target="_quran"}). When awarded their
records in their right hands, the successful will ecstatically run to
show them to their families, but the failures will cry out miserably
([89:7-11](https://quran.com/89/7-11){target="_quran"}).

These descriptions have penetrated deeply into the consciousness of
Muslims, who frequently draw comparisons between life and preparation
for an exam. While this imagery lends itself to the most sophisticated
and most naive understandings, every Muslim apprehends at least this
much: Life presents us with a continual series of tests and our
overall success or failure in responding to them will translate either
into a state of happiness or suffering in the life to come. It
supports the view that earthly life is an educational and
developmental stage in our creation.

## Sin as Self-Destruction {-}

If the purpose of life is to grow in the virtues that reach their
perfection in God so that we may receive and experience His attributes
to ever greater degrees -- to grow nearer to God in this sense -- and
if virtuous deeds promote this growth and evil deeds undermine it,
then it would follow, as we have already observed, that the one who
stands to gain or lose the most from a good or evil act is its
doer. This idea is stated explicitly in many places in the
Qur'an. Recall, for example:

> Taste suffering through fire in return for what your own hands have
wrought -- for never does God do the least wrong to His
creatures. ([3:182](https://quran.com/3/182){target="_quran"};
[8:51](https://quran.com/8/51){target="_quran"})

> Enlightenment has come from your God; he who sees does so to his own
good, he who is blind is so to his own
[hurt]. ([6:104](https://quran.com/6/104){target="_quran"})

> And whosoever is guided, is only (guided) to his own gain, and if
any stray, say: "I am only a warner."
([27:92](https://quran.com/27/92){target="_quran"})

> And if any strive, they do so for their own selves: For God is free
of all need from
creation. ([29:6](https://quran.com/29/6){target="_quran"})

> We have revealed to you the book with the truth for mankind. He who
lets himself be guided does so to his own good; he who goes astray
does so to his own
hurt. ([39:41](https://quran.com/39/41){target="_quran"}) (Also see
[10:108](https://quran.com/10/108){target="_quran"};
[17:15](https://quran.com/17/15){target="_quran"};
[27:92](https://quran.com/27/92){target="_quran"}

The statements in the Qur'an and in the traditions of the Prophet that
state that an evildoer's heart (his spiritual and moral sense) becomes
dark, veiled, rusted, hard, and hence impenetrable, and that the
hearts of the virtuous become soft, sensitive, and receptive to God's
guiding light, immediately come to mind.[^24] The verses in the Qur'an
that convey this idea most powerfully are those that assert that the
sinners destroy themselves by their wrongdoing -- that they commit
zulm (sin, wrong, harm, injustice, oppression) against themselves.

[^24]: See note 20 and, for example,
[2:74](https://quran.com/2/74){target="_quran"};
[5:13](https://quran.com/5/13){target="_quran"};
[9:87](https://quran.com/9/87){target="_quran"};
[18:57](https://quran.com/18/57){target="_quran"};
[22:46](https://quran.com/22/46){target="_quran"};
[2:54](https://quran.com/2/54){target="_quran"};
[26:89](https://quran.com/26/89){target="_quran"};
[33:53](https://quran.com/33/53){target="_quran"};
[39:23](https://quran.com/39/23){target="_quran"};
[64:11](https://quran.com/64/11){target="_quran"}; and
[83:14](https://quran.com/83/14){target="_quran"}.

> To Us they did no harm, but they only did harm to
themselves. ([2:57](https://quran.com/2/57){target="_quran"};
[7:160](https://quran.com/7/160){target="_quran"})

> If any transgress the limits ordained by God, then these, they wrong
themselves. ([2:229](https://quran.com/2/229){target="_quran"};
[65:1](https://quran.com/65/1){target="_quran"})

> And God did not wrong them, but they wronged
themselves. ([3:117](https://quran.com/3/117){target="_quran"})

> And so it was not God who wronged them, it was they who wronged
themselves. ([9:70](https://quran.com/9/70){target="_quran"};
[16:33](https://quran.com/16/33){target="_quran"};
[29:40](https://quran.com/29/40){target="_quran"};
[30:9](https://quran.com/30/9){target="_quran"})

> It was not We that wronged them: They wronged their own
selves. ([11:101](https://quran.com/11/101){target="_quran"})

> Oh My servants who have sinned against yourselves, never despair of
God's mercy. Surely God forgives all
sins. ([39:54](https://quran.com/39/54){target="_quran"})

Therefore sin, in reality, is a form of self-destruction. When we
commit it, we oppress and do injustice to ourselves, for we bar
ourselves from spiritual progress and deprive ourselves of that which
has real and lasting worth. As we saw earlier, and as the last verse
([39:54](https://quran.com/39/54){target="_quran"}) indicates, the
damage from wrongdoing does not have to be permanent, for the way to
reform is open. Personal reform involves repentance and making amends,
yet we should not lose sight of the most important element of all:
God's forgiveness.

When God forgives, He does much more than ignore or efface our sins.
He responds to our repentance and comes to our aid
([3:30](https://quran.com/3/30){target="_quran"}), He helps us repair
the harm that we inflicted upon ourselves
([33:71](https://quran.com/33/71){target="_quran"}), and guides us to
spiritual restoration
([57:28](https://quran.com/57/28){target="_quran"}). In the Qur'an,
the Divine Name "the Forgiving" is almost always paired with "the
Compassionate", and thus God's forgiveness involves embracing the
penitent with His compassion, which soothes the self-inflicted
wounds. The verb _tawbah_ (to turn toward) from the root _TWB_, brings
out the chemistry between repentance and forgiveness, for it is used
with various prepositions to describe both in the Qur'an.  When we
repent, we _turn toward_ God in repentance, seeking His mercy and
help, and He then turns toward us in His mercy, kindness, and
forgiveness. Thus God is described as _al Tawwaab_, the one who turns
toward others. God's forgiveness is His personal response to the
sinner, as in the saying of the Prophet which was quoted above: when
we go toward God walking, He comes toward us running.

The initiative, however, must come from the sinner. The first step
toward reform is the admission of wrongdoing, for we have to realize
and acknowledge the wrongfulness of our behavior and admit our need
for God's help in order to begin our recovery. This is similar to what
Alcoholics Anonymous counselors tell the desperate families of drug
addicts: Unless the addict admits that he has a problem and needs
help, no one can reform him. Sincerity is the key here. Recuperation
from sin is often a hard and painful process assisted by God's
forgiving involvement. It means starting over, going through the pains
of growth again, and entails work and effort. It is not a singular
moment or formula that repairs us, but our honest commitment to turn
our lives around and better ourselves. Thus the Qur'an states that
repentance at the last moment of life in order to escape suffering in
the next is ineffectual, because it is not motivated by a sincere
desire to reform and there is no time left for self-betterment.

> And repentance is not for those who go on doing evil deeds, until
when death comes to one of them, he says: Now I repent; nor for those
who die while they are ungrateful rejectors [of God]. For such we
prepared a great chastisement
([4:18](https://quran.com/4/18){target="_quran"}).

Pharaoh provides the archetypal case:

> And We brought the Children of Israel across the sea. Then Pharaoh
and his hosts followed them for oppression and tyranny, till, when
drowning overtook him, he cried: I believe that there is no god but He
in whom the Children of Israel believe, and I am of those who submit!
What! Now! And indeed before this you rebelled and caused depravity!
([10:90-91](https://quran.com/10/90-91){target="_quran"})

Not only is such last-second repentance vain and illustrative of a
complete lack of understanding of the purpose of life and of
repentance, but it incriminates the sinner all the more, because it
proves that he was always conscious of the existence of God, or at
least of the possibility of His existence, but preferred to live a
selfish and destructive life rather than seek a relationship with Him.

## Three Signs {-}

> And indeed He has created you by various
  stages. ([71:14](https://quran.com/71/14){target="_quran"})

The Qur'an presents three related analogies which have a bearing on
the meaning of human earthly existence. These are the _life in the
womb -- life on earth_, _birth -- resurrection_, and _death -- sleep_
analogies.

### Life in the womb -- Life on earth {-}

The Qur'an parallels two stages in our creation: our prenatal
development and our maturation after birth.

> Then certainly We create man of an extract of clay, then We place
him as a small quantity (of sperm) in a safe place firmly established,
then We make the small quantity into a tiny thing that clings, then We
make the tiny thing that clings into a chewed lump of flesh, then We
fashioned the chewed flesh into bones and clothed the bones with
intact flesh, then We cause it to grow into another creation. So
blessed be God, the best of creators! Then after that you certainly
die. Then on the Day of Resurrection you will surely be raised up.
([23:12-16](https://quran.com/23/12-16){target="_quran"})

> O people, if you are in doubt about the Resurrection, then surely We
create you from dust, then a small quantity (of sperm), then from a
tiny thing that clings, then from a chewed lump of flesh, complete in
make and incomplete, that We may make clear to you. And We cause what
We please to remain in the wombs until an appointed time, then We
bring you forth as babies, then after that you grow to
maturity. ([22:5](https://quran.com/22/5){target="_quran"}) (Also see
[40:67](https://quran.com/40/67){target="_quran"}.) .

> Does man think that he will be left aimless? Was he not a small
quantity (of sperm) emitted? Then he was a tiny thing that clings (in
the womb), and then He created (him), and then made him
perfect. ([75:36-38](https://quran.com/75/36-38){target="_quran"})

This parallel leads to a number of important insights. While our
prenatal growth is primarily physical, our earthly development is
principally moral and spiritual. As our birth into this life fully
manifests our physical maturation in the womb, our resurrection into
the next life fully manifests our current spiritual maturation in an
analogously objective way. Thus we find symbolic descriptions of the
Day of Judgment that indicate that our moral and spiritual doings on
earth will be manifested by our very being in the next. Our deeds will
be fastened to our necks
([17:13](https://quran.com/17/13){target="_quran"};
[34:33](https://quran.com/34/33){target="_quran"};
[36:8](https://quran.com/36/8){target="_quran"}).

Our tongues, hands, and feet will bear witness to our doings
([24:24](https://quran.com/24/24){target="_quran"};
[6:65](https://quran.com/6/65){target="_quran"}). We will eat the
fruits of our deeds
([37:39-68](https://quran.com/37/39-68){target="_quran"}). The
spiritually blind in this life will be raised without vision in the
next ([17:72](https://quran.com/17/72){target="_quran"}). Those who
lived in God's light in this life will have their lights shine before
them on the Day of Resurrection
([57:12](https://quran.com/57/12){target="_quran"};
[6:8](https://quran.com/6/8){target="_quran"}). Every deed of ours will
show its effect ([99:7-8](https://quran.com/99/7-8){target="_quran"}).

It is important to note that our creation is not presented as a single
moment in time, but as one that proceeds in stages. Our physical
development in the womb prepares us for our spiritual growth in the
next stage, which will determine the state of our being as we enter
the hereafter. Will there be opportunities for further growth in the
next life? Perhaps, for the Qur'an has the believers in heaven ask God
to "perfect for us our light"
([66:8](https://quran.com/66/8){target="_quran"}).

A popular American saying states that "you are what you eat". In other
words, one's diet greatly affects his or her physical well-being. A
Muslim might extend this to two more general truisms: "What you do in
this life determines what type of person you are", and, "in the next
life on the Day of Resurrection, you will be according to what you do
right now".

### Birth -- Resurrection {-}

> And people say: When I am dead, shall I truly be brought forth
alive? Does not man remember that we created him before, when he was
nothing? ([19:66-67](https://quran.com/19/66-67){target="_quran"})

The Qur'an contains an interesting reference to the two deaths which
we all experience:

> They say: Our Lord, twice you made us die, and twice you gave us
life, so we confess our
sins. ([40:11](https://quran.com/40/11){target="_quran"})

Some Qur'an exegetes felt that the first death corresponds to the
termination of non-existence at conception, but this is a strained
interpretation, since a death must naturally be preceded by a
life. Others believed that the first death represented the termination
of life in the womb at the moment of birth. This explanation seems
much more plausible, especially in light of the verses we just
considered and from what embryologists now know about the vast
differences between pre- and post-natal existence. (Even our
circulatory system reverses itself seconds after birth!)  The latter
viewpoint also complements the last parallel, for both deaths are
transitions to other levels of existence that are tied to our previous
developments.

Both stages of development -- in the womb and in life -- and the
corresponding ends of these stages involve pain and suffering. A
mother definitely experiences pain and suffering during pregnancy and
intense pain during birth. The fetus also experiences times of
discomfort in the womb and certainly undergoes great hardship at
birth.

What I find remarkable is that only minutes after the birth, the
mother, and even much more so the child, appear to forget the
tremendous agony that they have just endured. I recall how exhausted
and drained I was after each of my daughters' births and yet how
quickly my wife and daughters recovered, even though their suffering
was incredibly more severe. My children appeared to have no
recollection or after effects from the ordeal. Perhaps there is a sign
in this concerning those who enter a state of paradise in the next
life. Will all their earthly agony and hardship suddenly seem to them
like an illusion, a dream, even though it was all very real? The next
parallel we consider suggests that this may be so.

### Death -- Sleep {-}

> God takes souls at the time of their death, and those that do not
die, during their sleep. Then He withholds those on whom has passed
the decree of death and sends the others back till an appointed
term. Truly there are signs in this for a people who
reflect. ([39:42](https://quran.com/39/42){target="_quran"})

The Resurrection moment, as pictured in the Qur'an, is very much an
arousal from a deep slumber. A trumpet blast will awaken the dead
([6:73](https://quran.com/6/73){target="_quran"}). The disbelievers
will rush from their graves, which the Qur'an refers to as their
"sleeping places", in terror. People will be groggy and swoon
([39:68](https://quran.com/39/68){target="_quran"}). They will be
disoriented. Their earthly lives will seem like an illusion
([27:88](https://quran.com/27/88){target="_quran"}). There will be
mass confusion over the time spent on earth: to some it will seem like
ten days and to others like a day or even less
([20:103-104](https://quran.com/20/103-104){target="_quran"};
[23:113](https://quran.com/23/113){target="_quran"}), just like when
one recollects a dream, details will be very hazy. Peoples' sight will
be confused, like when one arises from sleep
([75:7](https://quran.com/75/7){target="_quran"}), and then their
vision will sharpen and they will have a keen grasp of reality
([50:20](https://quran.com/50/20){target="_quran"}). The righteous
appear to have only faint recollection of their earthly struggles, and
the damned have only faint recollection of their earthly pleasures. A
well-known saying of Muhammad states that if one of the faithful is
immersed in Paradise and is then asked about all his suffering on
earth, he will not be able to recall any of it, and if one of the
sinful is immersed in Hell and then is asked about all his earthly
pleasures, he will not remember any of them.[^25]

[^25]: From _Sahih Muslim_ as translated in _Riyad as Salihin_ of Imam
    Nawawi, 103.

Hence our lives on earth will seem like a dream. All the pain,
struggle, and agony, which appeared so hard and enduring, will be no
more than a vague, distant, brief memory, something like when one
awakens from a nightmare. A bad dream is very real while we are
experiencing it, but when we awake, we feel immediate relief because
we are now conscious of a greater reality. It seems that the
resurrection of the righteous will be a somewhat similar but more
intense experience. Our earthly lives are not dreams nor illusions;
what we experience is very real and the consequences of our deeds will
become marked upon our souls -- written and recorded upon our being --
but, by the mercy of God, the hardships true believers endured will be
erased from their recollection
([35:35](https://quran.com/35/35){target="_quran"}). Like the newborn
child, their previous existence is forgotten, although they carry with
them into the next life their earlier development.

## "Except That They Should Worship Me!" {-}

> Say: Truly my prayer and my sacrifice and my living and my dying --
all belong to God, the Lord of the
worlds. ([6:163](https://quran.com/6/163){target="_quran"})

Islam's concept of worship complements its view of life. I recall a
conversation I had not long ago with a friend who asked me how Muslims
worship. I told her that we go to work to provide for our families,
attend school functions that our children are involved in, take a few
pieces of cake we just baked over to our neighbor next door, drive our
children to school in the morning.

"No! No!" She said. "How do you worship?"  I said we make love to our
spouses, smile and greet someone we pass on the street, help our
children with their homework, hold open a door for someone behind us.
"Worship! I'm asking about worship!" She exclaimed.  I asked her
exactly what she had in mind. "You know -- Rituals!" She insisted.  I
answered her that we practice those also and that they are a very
important part of Muslim worship. I was not trying to frustrate her,
but I answered her in this way in order to emphasize Islam's
comprehensive conception of worship.

A famous tradition of the Prophet states that a Muslim is responsible
for at least one _sadaqah_ daily for every joint in his/her body. The
word _sadaqah_ is often translated as "charity." It is derived from
the same root as the Arabic verb "to be truthful or sincere" and thus
most generally signifies an act of fidelity or sincerity towards
God. Hence, to Muslims, an act of _sadaqah_ is a form of worship.

When Muhammad made this statement to his Companions, they felt
overwhelmed, for how can anyone perform so many acts of piety each
day?  He responded to them that to act justly is a _sadaqah_, to help
a rider on his mount is a _sadaqah_, and to remove a stone from the
road to ease the way for other travelers is a _sadaqah_[^26] On other
occasions, he mentioned that smiling at another person, bringing food
to one's family, and making love to one's spouse are all pious acts.

[^26]: Ibid., 59.

Muhammad's Companions expressed shock at the last of these, since it
brings such carnal satisfaction. So he asked them if they did not
consider adultery sinful and harmful, and when they responded that
they did, he asked them why were they surprised that marital romance
was a meritorious act in the service of God.[^27] The Prophet's
followers wondered what, then, were the greatest acts of faith? He
included on different occasions: fighting in a just cause, standing up
to a tyrant, taking care of your parents in their old age, giving
birth to a child -- and if a mother should die while giving birth,
then she ranks amongst the highest witnesses of faith.

[^27]: Ibid.

To the Muslim, almost every moment of life presents an opportunity for
worship, and he or she aspires to transform all of his/her earthly
life into a type of continuous prayer as verse
[6:163](https://quran.com/6/163){target="_quran"} has the Muslim say:
"Truly my prayer and my sacrifice and my living and my dying -- all
are to God." This idea is ingrained deeply in the Muslim character,
and so we find believers dedicating even their simplest actions to God
with the formula, "In the name of God, the Merciful, the
Compassionate."

An Egyptian cab driver about to start his car, a Moroccan mother
reaching to pick up her crying child, and a Pakistani worker raising a
glass of water to his lips, will pronounce
_"bismillah-ir-rahman-ir-raheem"_ (In the Name of God, the Merciful,
the Compassionate). Every healthy and wholesome activity has the
potential to be a worshipful act. Any good deed, performed by one who
strives to surrender his/her life to God, can become a moment of
devotion. A believer knows that his/her inner peace, happiness,
growth, and prosperity correspond to the level of self-surrender
he/she attains. To him or her, worship then becomes synonymous with
doing what is good and ultimately personally beneficial.

Many western scholars of Islam have objected to the verse in the
Qur'an where God states "I have not created _jinn_ (beings beyond
human perception) nor man except that they should worship me"
([51:56](https://quran.com/51/56){target="_quran"}), seeing an
infinitely jealous and capricious narcissism -- the worst side of the
Old Testament depictions of God. Yet a Muslim, possessing his/her
understanding of the purpose of life and possessing this very general
and broad concept of worship, will read the very same verse and
respond: "But of course, for what other purpose could there possibly
be?"

## Additional Questions {-}

We have traveled far, yet in some ways, it seems as if we never needed
to leave. Like Dorothy in the Wizard of Oz, we had to venture over the
rainbow in order to discover that the key to happiness is within
ourselves. To many of those who have recently accepted Islam, its view
of life is so holistic and natural that they are astonished that they
had not conceived of it themselves.

We saw that the Qur'an claims that earthly life is an essential stage
in our creation. It represents a learning stage in which we can
develop our intellectual and spiritual qualities and increase in our
capacity to know, receive, and experience the attributes of God so
that we can enter into a relationship of love with Him that no human
relationship could approximate. We observed that human reason, choice,
and suffering are key ingredients in this stage and that our
relationship with others is organically linked to our relationship
with God. We witnessed that human error, sin, and repentance, together
with God's forgiveness and His continuous, pervasive influence, aid us
in our growth. For many of us, most of the objections we raised
initially have dissolved along the way and, although we may still have
unanswered questions, we are coming to realize that they are due to
the limitations of our own reasoning -- our inability to fully
comprehend the truth before us -- and that given enough time, thought,
and study of the Qur'an, we may be able to find satisfactory answers.

We are not at our journey's end. The remainder of the book is devoted
to sharing as best its author can the rest of that travel to Islam in
America. Ahead, we will meet the inevitable decision that the Qur'an
demands of us. Then we will consider the five pillars of Islam and the
support they provide to those who have made the decision to
convert. We will also meet the community of believers and the tests
that its members can bring to one's sincerity. Finally, we look
briefly at the future of Muslims in America. Before we continue on,
however, we will consider a few more of the theodicy questions that
are often raised by modern-day Muslims and non-Muslims interested in
Islam. I chose to discuss those about which I am most often
asked. Answers to some of them follow as easy corollaries of what we
already discovered, while others require a fresh look at the Qur'an
from different angles. Some of them are discussed elsewhere,[^28] and
I will repeat -- sometimes word for word -- what I wrote there. They
are included here for the sake of completeness.

[^28]: Lang, _Struggling to Surrender._

### On Omnipotence {-}

If God is all-powerful, can He become a man, terminate His existence,
tell a lie, be unjust, or create a stone too heavy for even Him to
move? These somewhat silly riddles most often arise from imposing
unnecessary and contradictory assumptions on certain attributes of God
or by assigning unwarranted additional attributes to Him. For example,
the Qur'anic concept of omnipotence is not that God can do any
arbitrary thing at all, even though it defies all laws of logical
truth. Instead the Qur'an states that God "has power over all things"
([2:20](https://quran.com/2/20){target="_quran"};
[3:29](https://quran.com/3/29){target="_quran"};
[55:17](https://quran.com/55/17){target="_quran"};
[6:17](https://quran.com/6/17){target="_quran"};
[16:77](https://quran.com/16/77){target="_quran"};
[67:1](https://quran.com/67/1){target="_quran"}) and thus it is
impossible that a created object could exist beyond or independent of
His power, such as a stone too heavy to be moved.

Creation is also subject to and in harmony with His attributes. While
God does "whatsoever He wills"
([2:253](https://quran.com/2/253){target="_quran"};
[5:1](https://quran.com/5/1){target="_quran"};
[11:107](https://quran.com/11/107){target="_quran"};
[22:14](https://quran.com/22/14){target="_quran"}), what He wills is
not arbitrary or capricious but in accord with His Most Beautiful
Names. Hence, it is outside of His perfection to do ridiculous or
stupid things. Similarly, His attributes are not in conflict with each
other. If omnipotence included the ability to become a man, to
terminate oneself, to lie, or be unjust, then His name the
All-Powerful would be in conflict with the names the Absolute (_al
Samad_), the Everlasting (_al Qayyum_), the Truth (_al Haqq_), or the
Most Just Judge (_al Hakeem_), respectively. Therefore, the obvious
answer to each of the above riddles is that the Qur'anic concept of
omnipotence does not include these acts.

The problem of predestination involves similar but more subtle logical
traps that arise from imposing time constraints on God.

### Predestination {-}

The concepts of time and eternity and their relationship to God have
been the subject of diverse philosophical speculation throughout the
history of religion. This is amply demonstrated in Muhammad Iqbal's
_Reconstruction of Religious Thought in Islam_.[^29] where he attempts
a new interpretation in conformity with modern thought and doctrinal
sources of Islam. The attempt itself has been widely praised by Muslim
and non-Muslim scholars, although there is considerable disagreement
in both camps about the validity of his ideas. We should, Iqbal
asserts, not underestimate the importance of such efforts, since many
theological paradoxes arise from our understanding of these
concepts. On the one hand, we cannot, as scriptures themselves cannot,
resist relating time to God. On the other hand, we must alert
ourselves to the deficiencies of our understanding.

[^29]: Muhammad Iqbal, _The Reconstruction of Religious Thought in
    Islam_ (Lahore, Pakistan: Sh. Muhammad Ashraf Publ., 1982).

The greatest perplexities arise by attributing to God human
restrictions in relation to time. As God transcends space, we
naturally would not associate with Him spatial limitations. For
instance, we would not say that God literally descends to earth or
walks in the garden. Equally, we would not insist that God is a
three-dimensional being or that He travels from one point in space to
another. In the same way, we should not demand that God have a past,
present, and future, for this assumes that His existence is as ours,
_in time_, and again this conflicts with His transcendence. Even the
most rabid atheist, in an effort to prove the illogic of the concept
of God, would not suppose that God might be on a bus from Chicago on
His way to New Orleans, because he knows that such a hypothesis is
unacceptable to a believer. It is equally erroneous to assume God's
being was confined to a particular point or interval of time.

We have little difficulty accepting the idea that God's knowledge can
encompass two different points in space simultaneously. This is
perhaps because we assume that the attribute of transcending space
implies a unique vantage point. We could compare it, however
imperfectly, to the experience of being high above the ground and
having simultaneous knowledge of very distant events. With respect to
time, unlike space, we are immobile.  We can not travel forward or
backward in time. An hour from now, we will be at an hour from
now. That can not be changed. Therefore, it is more difficult to
comprehend that God's existence is independent of or beyond time, as
indeed it must be, for it is impossible to conceive that His being is
contained within or constrained by any of the dimensions of the very
spacetime environment that He created for us to live and grow in. Once
again, because of His unique vantage, point, His knowledge encompasses
all events, regardless of their distance in space or time.

Another key point, well established in the Qur'an, is that our
perception of time is not objectively real. As noted earlier, the Day
of Judgment is portrayed as a different order of creation, one in
which we suddenly comprehend that our former perceptions of time are
no longer valid.

> The Day they see it, (it will be) as if they had tarried but a
single evening, or the following
morning. ([79:46](https://quran.com/79/46){target="_quran"})

> One Day He will gather them all together: (it will seem) as if they
had tarried but an hour of a
day. ([10:45](https://quran.com/10/45){target="_quran"})

> It will be on a Day when He will call you, and you will answer with
His praise, and you will think that you tarried but a little
while. ([17:52](https://quran.com/17/52){target="_quran"})

> In whispers will they consult each other: "You tarried not longer
than ten (Days)."
([20:103](https://quran.com/20/103){target="_quran"})

> "You tarried not longer than a day."
  ([20:104](https://quran.com/20/104){target="_quran"})

> He will say: "What number of years did you stay on earth?" They will
say, "We stayed a day or part of a day: ask those who keep account."
He will say: "You stayed not but a little, if you had only known!"
([23:112-13](https://quran.com/23/112-13){target="_quran"})

> On the Day that the Hour will be established, the transgressors will
swear that they tarried not but an hour: thus were they used to being
deluded. ([30:55](https://quran.com/30/55){target="_quran"})

Interpreters will always render all references to the Day of Judgment
in the future tense, because from our perspective that is when it will
take place. Several of the passages, however, actually employ the
present and past tenses. Commentators assert that this is a literary
device that stresses the inevitability of these happenings, but it
seems that the use of the present and past tenses in referring to the
Day of Judgment also reinforces the notion that it will take place in
a very different environment, one in which our current conceptions of
time and space no longer apply.

The illusory character of time is further supported by the Qur'an's
comparisons of the "days of God" with earthly days, where a "day of
God" is said to be as "a thousand years of your reckoning"
([32:5](https://quran.com/32/5){target="_quran"}) and like "50,000
years" ([70:4](https://quran.com/70/4){target="_quran"}). I will not
attempt here to provide a model or to interpret the precise
relationship between God and time, or for that matter God and space,
but rather I suggest the futility of such an endeavor. It cannot be
otherwise, since our perceptions of time are not objectively
real. Conflicts arise precisely because a given interpretation is
assumed.

The question "What is the value of prayer if God has already
predestined the future?" assumes that in some way God has a
future. That is, it assumes that God is situated in time, as we are,
and that as we pray, He is peering into a preordained future. But in
order to have a future, one's existence must be contained within --
and hence finite -- in time.  The reason this question leads to
contradictions is that it assumes a contradiction in the first place:
that God both transcends and is finite in time.

Any question that assumes two mutually incompatible premises will
always result in conflicting conclusions. Assume, for example, that a
circle is a square. We may ask if a circle has corners. If we
emphasize the circle's roundness, the answer is no. If we concentrate
on the properties of a square, the answer is yes. When two assumptions
lead to a contradiction, at least one of them must be false. Thus, in
such a situation, it should be asked if the question itself makes
sense and if all premises are necessarily true.

The word "predestination" is itself problematic. If we mean by it that
God in the past had programmed the events of the future, the
assumption is that God exists in time. If instead we mean that God's
wisdom, knowledge, and power encompass all and nothing in creation can
conflict with that, then that has to be admitted. However, this is not
the primary sense of "predestine", which means "determine in advance",
and it does not conflict with the notion that God responds to our
prayers.

The words _qadar_ and _taqdir_ in the Qur'an have come to mean, for
many Muslims and Orientalists, the "absolute decree of good and evil
by God". That is, that God has preordained all our acts, even our
moral choices. But as Muhammad Ali argues, this doctrine is

> neither known to the Holy Qur'an, nor even to Arabic lexicology. The
doctrine of predestination is of later growth, and seems to have been
the result of the clash of Islam with Persian religious thought.[^30]

[^30]: Muhammad Ali, _The Religion of Islam_ (New Delhi: S. Chand &
    Co., 317-18.

According to Raghib,[^31] _qadar_ and _taqdir_ mean "the making
manifest of the measure of a thing" or simply "measure". In the Qur'an
they signify the divine laws regulating and balancing creation.

[^31]: Ibid.

> Glorify the name of your Lord, the Most High, Who creates, then
makes complete, and Who makes things according to a measure
(_qaddara_, from _taqdir_), then guides them to their
goal. ([87:1-3](https://quran.com/87/1-3){target="_quran"})

> Who created everything, then ordained for it a measure
(_qadar_). ([54:49](https://quran.com/54/49){target="_quran"})

> And the Sun runs on to a term appointed for it; that is the law
(_taqdir_) of the Mighty, the Knowing. And as for the moon, We
ordained (_qaddarna_ from _taqdir_) for it stages.
([36:38-39](https://quran.com/36/38-39){target="_quran"})

> Of what thing did He create him [man]? Of a small lifegerm He
created him, then He made him according to a measure
(_qaddarahu_). ([80:18-19](https://quran.com/80/18-19){target="_quran"})

This is not to claim that God has subjected the universe to certain
scientific laws and abandoned it to let it run its course. No reader
of the Qur'an gets this impression. In the Qur'an, God is _al Rabb_:
the Sustainer, Cherisher, Regulator, and Governor of all. He is the
omnipresent source of the harmony and balance of nature. His influence
and sway over creation is continuous and all-pervading. However, none
of this conflicts with the fact that we are empowered to make moral
decisions and to carry them out or that God comes to our aid when we
seek Him.

### On the Origins of Evil and Temptation {-}

Where does evil come from? If it comes from God, then it implies that
God is imperfect; if it does not, then it implies that something can
come into existence independent of Him. Since we already touched on
this question when we considered human choice, we will summarize
quickly our earlier observations and include some additional comments.

As we have seen, evil is not absolute, existing independent of and in
eternal conflict with God. It arises from human nature, which is
suited to moral and spiritual growth. What we consider to be evil --
tyranny, oppression, deceit, injustice, greed, indifference to the
suffering of others -- is an outcome of human choice. It is a
rejection of and opposition to the divine attributes and our own best
interests. This helps explain the Qur'an's description of the
disbelievers as _kuffar_, a term that connotes one who shows
ingratitude or rejects a gift. Human intelligence and volition, when
confronted with the challenges presented by earthly life, frequently
will choose evil. However, the same key ingredients combine in some of
us to produce remarkable exemplars of goodness. As the Qur'an states,
man's capabilities -- in particular, his ability to sin and do evil --
are from God. He empowers us to choose evil just as He empowers us to
choose good. But the choice is ours, and it is in that choice that
good or evil occurs:

> Say: "All things are from God." But what has come to these people,
that they fail to understand a single fact? Whatever good befalls you
is from God, but whatever evil befalls you, is from
yourself. ([4:78-79](https://quran.com/4/78-79){target="_quran"})

To choose evil is self-defeating, as the wrongdoer sins -- commits
_zulm_ -- against himself. But the damage does not have to be
permanent, for through sincere repentance, making amends, and with
God's forgiveness and help, we can learn and grow from our
mistakes. The existence of and our ability to choose evil as well as
good is an essential element of this learning phase of our
creation. Evil in this life is not in conflict with God, but rather
serves His purposes for mankind. The same could be said about the
existence of temptation.

Our decisions are based not only on sensory data. All peoples of all
times have been aware of extrasensory influences that introduce subtle
suggestions into the human mind. In the past, the study of these
forces fell exclusively within the province of religion, while today,
modern psychology dominates their study. Religions tended to view
these psychic influences as independent from man, but modern science
believes them to belong to a subliminal region of our minds. I will
not attempt here to resolve this difference in viewpoint or to
harmonize Islam with any recent theories of psychology. My interest is
not in the precise origin, development, or location of these
influences -- frankly, I believe that this will always be a mystery to
science -- for my aim is only to discuss the role of temptation in
man's development. Of course, the Arabs of the Prophet's era had their
own pneumotology and vocabulary for describing psychic phenomena and,
quite naturally, the Qur'an adopted and adapted this system to its
calling. In order to better understand the purpose of temptation, it
will be helpful to review some terminology.

The word _jinn_ was to the ancient Arabs a comprehensive term for
beings and powers outside of their immediate experience or
perception. It is derived from the Arabic verb _janna_, meaning "to
cover, conceal, hide, or protect". To Muhammad's contemporaries, it
denoted "a being that cannot be perceived with the senses".[^32] The
Arabs, as pointed out by Muhammad Ali, commonly referred to other
humans as _jinn_. He quotes famous Muslim lexicologists who explain
that it could be used to designate _mu'zam al nas_, i.e., "the main
body of men or the bulk of mankind".[^33]

[^32]: Ibid., 188.
[^33]: Ibid., 191.

> In the mouth of an Arab, the main body of men would mean the
non-Arab world. They called all foreigners _jinn_ because they were
concealed from their eyes.[^34]

[^34]: Ibid., 191-92.

Through the centuries a tremendous amount of superstition and folklore
grew around this word in the Middle East and the Far East, as well as
the two other terms that we are about to discuss. Although these
developments make it difficult to know exactly what it meant to the
Qur'an's first hearers, it appears that any imperceptible being might
be referred to as a _jinn_. From its use in the Qur'an and the
Prophet's traditions, however, it seems that the term was used most
often to refer to a world of spirits, one of sentient beings who were
invisible to mankind but yet influenced and sometimes interacted with
men.

A close relative of _jinn_ is the word _shaitan_, usually translated
as "satan". In general, _shaitan_ stands for any rebellious being or
force. In his famous commentary on the Qur'an, al Tabari states that

> _Shaitan_ in the speech of the Arabs is every rebel among the
_jinn_, mankind, beasts, and everything .... The rebel among every
kind of thing is called a _shaitan_, because its behavior and actions
differ from those of the rest of its kind, and it is far from
good.[^35]

[^35]: Al Tabari, _The Commentary on the Qur'an_, trans. by J. Cooper
(Oxford: Oxford University Press, 1987), 1-47.

It derives its meaning from a verb which means to be remote or
banished.

> It is said that the word is derived from [the use of the 1st form
verb _shatana_] in the expression _shatana dar-i min dari-k_ (=My home
was far from yours).[^36]

[^36]: Ibid., 47.

As al Tabari points out, _shaitan_ can be applied to humans. He quotes
Ibn'Abbas in his commentary on
[2:14](https://quran.com/2/14){target="_quran"}:

>There were some Jewish men who, when they met one or several of the
Companions of the Prophet, may God bless him and grant him peace,
would say: "We follow your religion." But when they went in seclusion
to their own companions, who were their satans, they would say: "We
are with you, we were only mocking."[^37]

[^37]: Ibid., 131.

He also quotes Qatadah and Mujahid, who claimed that these satans were
"their leaders in evil" and "their companions among the hypocrites and
polytheists".

The key difference between a _jinn_ and a _shaitan_ is that while the
former could be benign or destructive, the latter is always evil. In
particular, in the pneumatic realm, a _shaitan_ is an evil or
rebellious _jinn_. However, the power of Satan in the Qur'an is rather
limited: He is a source of evil suggestions that enter a person's
hearts ([114:4-6](https://quran.com/114/4-6){target="_quran"}) and the
notorious tempter, but beyond that the Qur'an states that he has no
authority over man ([14:22](https://quran.com/14/22){target="_quran"};
[15:42](https://quran.com/15/42){target="_quran"};
[16:99](https://quran.com/16/99){target="_quran"};
[17:65](https://quran.com/17/65){target="_quran"}) and that his guile
is weak ([4:76](https://quran.com/4/76){target="_quran"}).

> And Satan will say, when the matter is decided: Surely God promised
you a promise in truth, and I promised you, then failed you. And I had
no power over you, except that I called you and you obeyed me; so
don't blame me, but blame yourselves. I can not come to your help, nor
can you come to my help. I reject your associating me with God
before. Surely for the unjust is a painful
chastisement. ([14:22](https://quran.com/14/22){target="_quran"})

Satanic temptation is counterbalanced in Islamic pneumatology by
angelic inspiration. The angels (in Arabic, _malaa'ikah_;
sing. _malaak_), among other things, encourage and support virtuous
deeds in men and women. From the Qur'an, the sayings of the Prophet,
and ancient dictionaries, it seems that, unlike the two previous
terms, the word _malaak_ (angel) applied only to spiritual beings. The
Arabs also had a number of firmly held beliefs concerning angels that
the Qur'an rejects: that angels are daughters of God
([16:57](https://quran.com/16/57){target="_quran"}) and hence
semidivine, or that angels are female creatures
([17:40](https://quran.com/17/40){target="_quran"};
[37:150](https://quran.com/37/150){target="_quran"};
[3:21](https://quran.com/3/21){target="_quran"}).

These three terms characterize the pneumatic powers that influence the
psyche. Angels, satans, and _jinns_ account for many of the virtuous,
harmful, and ambivalent psychic urgings to which we are
exposed. Angels inspire magnanimity and self-sacrifice. Satans are a
source of evil and self-destructive suggestions. The influence of
_jinns_ could be either positive or negative depending on how we deal
with them, for they excite our lower or more animalistic tendencies,
such as our drives for self-survival, power, wealth, security, and the
respect of others. Their relationship and function is described
succinctly in a well-known saying of Muhammad. He stated that every
human is created with a companion _jinn_, who excites his lower
passions, and a companion angel, who inspires him with good and noble
ideas. When Muhammad's audience asked if he too had a companion
_jinn_, he responded, "Yes, but God helped me to overcome him, so that
he has submitted and does not command me to anything but good."[^38]

[^38]: Ahmad ibn Hanbal, _At Musnad_ (Cairo: al Maimanah Press, n.d.),
1:385, 397, 401.

The virtuous and base promptings we receive could balance and
complement each other. Magnanimous urgings, which stimulate our moral
and spiritual growth, would, if surrendered to completely, be
self-destructive, for they would cause us to ignore our personal
needs. Base desires are necessary for our earthly survival, but if we
gave in to them entirely, we would become utterly selfish. The two
work together to stimulate our moral and spiritual growth, since what
makes an act virtuous is that it involves overcoming or putting aside
our lower needs for a while. The successful person, as Prophet
Muhammad's saying shows, is the one who can discipline these lower
(_jinnic_) influences and balance them against angelic ones. Then both
serve his growth in goodness.

When a person inclines too far towards these lower (_jinnic_)
suggestions, he makes himself or herself easy prey to evil (satanic)
influences. For example, our need to survive gives way to exploitation
of others and avariciousness, our need for power gives way to tyranny,
our need for wealth gives way to greed, our desire for security gives
way to violence, and our wish for respect gives way to arrogance. Such
a person, as we saw earlier, becomes spiritually self-destructive,
thereby making satanic influences an "obvious enemy" to man
([2:168](https://quran.com/2/168){target="_quran"};
[7:22](https://quran.com/7/22){target="_quran"};
[12:5](https://quran.com/12/5){target="_quran"};
[35:6](https://quran.com/35/6){target="_quran"}).

These three psychic influences usually act upon us simultaneously.
Thus they pinpoint and heighten the morality of many decisions and
together provide a stimulus and catalyst for spiritual
development. From the standpoint of Islam, what we call temptation is
only one type of extrasensory influences to which we are exposed, and,
in combination with the others, it perpetuates and hastens our
growth. Like all other aspects of our earthly lives, it is in harmony
with God's plan for us.

It has been pointed out to me on occasion that this outline compares
to certain theories in modern psychology, in particular, to Freud's
description of the id, ego, and superego. This may be the case, but I
find it neither surprising nor something to be excited about. First of
all, if there are similarities between the two systems, there are
obviously major differences. Second, I do not consider Freud's ideas
to be truly modern nor a discovery in the strictest sense, because the
viewpoint just presented is part of ancient wisdom and is contained in
many religious traditions. What Freud attempted to do was to construct
a secular context for explaining and investigating psychic
influences. I, on the other hand, am writing as a convert to Islam
from a definite religious perspective.

### Don't We Need Another Prophet? {-}

> This day have I perfected for you your religion and completed My
favor to you and chosen for you Islam as a
religion. ([5:3](https://quran.com/5/3){target="_quran"})

> Muhammad is not the father of any of your men, but he is the
Messenger of Allah and the Seal of the prophets. And Allah is ever
Knower of all
things. ([33:40](https://quran.com/33/40){target="_quran"})

The second verse, revealed in the fourth year after Muhammad's
emigration to Madinah, turned out to contain a prophecy as well as a
statement of current fact. Muhammad would leave no male heirs and thus
no natural candidate to whom the community might look as inheritor of
the prophetic mantle. The deep emotional and psychological need for
such a person was indeed felt. On the day of the Prophet's death,
Umar, one of his leading Companions, and many others refused to accept
that the Prophet had truly passed on until Muhammad's closest friend,
Abu Bakr, brought them to their senses. In the succeeding years, there
were many others who sought from Muhammad's family a divinely guided
leader, one who, through kinship, would be endowed with charismatic
authority. But the Qur'an, history, and the Prophet's decisions in his
last days made such a search difficult.

Not only did he leave no sons, but Muhammad outlived all of his
daughters except Fatimah, the youngest, who died very shortly after he
did. Had the Prophet designated Fatimah's husband and his nephew, Ali,
as his successor, he might very well have been raised to divinely
ordained status in the eyes of most of the Muslims. Muhammad, however,
apparently left it to the community to select their next leader, and
Ali would not be elected until three political successors to Muhammad
(Abu Bakr, Umar, Uthman) had already reigned and then died. Either of
Ali and Fatimah's two sons would have been likely candidates, but both
of them died -- the younger, tragically, while opposing the sixth
ruler after Muhammad -- without attaining political rule. Nonetheless,
various lines of descent from Ali came to be viewed by a significant
Muslim minority as inheriting a prophetic charisma, although most
viewed the leadership of Muslims as simply a political appointment
having religious significance and responsibilities but no divine
mandate.

If Muhammad had been outlived by a son or perhaps a daughter -- for
the Arabian peninsula had known queens in pre-Islamic times -- for a
sufficient length of time, or if a grandchild of his had obtained
political leadership, Muslim political history could possibly have
been very different. But as it was, Muhammad's refusal to appoint a
political successor and the fact that no direct descendent rose to
power in the first few decades after his death, helped to insure that
the majority of Muslims would understand the designation, "the Seal
(_al khaatam_) of the Prophets", in the most obvious and conservative
sense.

The Qur'an states that from every people God chose at least one
prophet at some time in their history
([10:47](https://quran.com/10/47){target="_quran"};
[13:7](https://quran.com/13/7){target="_quran"};
[16:36](https://quran.com/16/36){target="_quran"};
[35:24](https://quran.com/35/24){target="_quran"}). It also contains
examples of nations to which prophets were sent repeatedly, because
the divine message they conveyed would inevitably be distorted or
forgotten. In some traditions of Muhammad, the number of prophets
chosen by God for mankind is said to number as many as one hundred
thousand. We are told that Muhammad's mission was also corrective and
restorative: the revelation he communicated confirms the fundamental
truths contained in other sacred books -- principally the Jewish and
Christian scriptures -- and corrects key errors
([2:91](https://quran.com/2/91){target="_quran"};
[6:92](https://quran.com/6/92){target="_quran"};
[35:31](https://quran.com/35/31){target="_quran"}).

This is a rather pessimistic appraisal of mankind's spiritual and
moral resolve. Since its very beginnings, the human race has been
consistently guilty of forgetfulness and perversion as well as of an
inability to preserve and adhere to God's revelations. So that even if
the Qur'an, as Muslims claim, is the same revelation proclaimed from
Muhammad's lips, preserved in its original language, and free from
later editing and revision, are we not, like most of humanity
throughout history, in need of another prophet? In other words, does
it make sense that God would suddenly leave humanity, with its proven
propensity for deviation from revelation, to itself until the end of
creation after guiding man so directly up to and including the time of
Muhammad?

The Muslim might counter that the Qur'an is distinguished from all
other sacred scriptures by its purity; others may contain sayings
close to what earlier prophets preached -- maybe even some verbatim
statements -- but these are mixed so thoroughly with folklore, poetry,
interpretation, commentary, along with errors in translation, copying,
editing and transmission, and with other accretions, that sifting out
the actual revelation has long been impossible. The Muslim insists
that the Qur'an, on the other hand, contains nothing but the words
proclaimed by Muhammad during the times when revelation descended upon
him. Most modem non-Muslim scholars of Islam are willing to admit this
much, or at least something very close to this. Thus, this argument
goes, we now possess the unadulterated word of God to guide us, making
another revelation or prophet unnecessary. Other religions, of course,
have developed viewpoints on the purpose and means of revelation and
scripture quite different from the Muslim understanding. It is not my
intention to contrast or debate them; I only wish to present a common
explanation for the finality of Muhammad's mission.

Does this explanation fully answer the above question? What about the
need to interpret and apply the revelation in an ever-changing world
and the many difficulties which are bound to arise in time not
explicitly addressed by the scripture? The Muslim replies that we have
the Prophet's life example, his _Sunnah_, the multitudinous
recollections of his sayings and doings that were collected, collated
and subjected to meticulous historical criticism during the first
three Islamic centuries.

Yet certainly we will encounter situations not dealt with in the
Prophet's lifetime -- after all, it has been over fourteen hundred
years since his demise. How do we respond correctly to these? The
Muslim answers that we have the sacred law, the Shari'ah, based on the
Qur'an and the Prophet's _Sunnah_: a complete code of living,
developed over several centuries by jurists to meet every possible
contingency.

Is it any longer tenable to claim that the ancient jurists foresaw
every possible future problem? Of course not, admits the Muslim, but
we can study their methodologies, repeat their effort, and derive
Islamic rules and regulations to address our current circumstances.

This is the inevitable response, but the farther we move away from the
Qur'an, the greater our dependence on human choices and judgments,
which, it seems, are bound to differ and to include errors. Today,
Muslims are debating and often quarreling among themselves over
hundreds of issues as they strive to adapt to modem
living. Admittedly, these arguments are not over concepts central to
Islam -- they involve almost exclusively what non-Muslims would regard
as mundane issues: men's and women's roles in the community, banking
and investment practices, relations with non-Muslims, Muslim
involvement in western political systems and similar concerns -- but
they are extremely important to the community. Such controversies
create a great deal of strain and dissension. As one Muslim student
attending the University of Kansas once told me, "If only the Prophet,
peace be upon him, were here today to settle these issues for us!"
From this perspective then, do we not need another prophet?

Any answer is purely speculative, because the Qur'an does not
explicitly respond to this question. There very well may be many
reasons rather than a single rationale. It is possible that the
collective attempt to work out a program of living guided by the
Qur'an and Muhammad's life example is in itself a valuable social,
intellectual, and spiritual exercise requiring cooperation, tolerance,
humility, and sincerity. The possibilities for growth that such an
endeavor holds may outweigh the benefits of having a prophet to decide
every small point of difference.

Another factor might be that the current environment is incapable of
producing an individual having the level of purity and simplicity
needed to be a prophet. Perhaps life has become so complicated and
corrupting that no one of us is any longer capable of attaining the
spiritual sensitivity and receptivity of Moses, Jesus or
Muhammad. This interpretation brings to mind the many traditions of
the Prophet that predict that life will become increasing corruptive,
such as, "The best of my community is my generation, thereafter those
who follow them, thereafter those who follow them. Then will come
[such] people that one's testimony will outrun his oath, and one's
oath his _testimony_."[^39] Recall also the statement in the Qur'an
that asserts that while many in Muhammad's era excel in faith, very
few will do so in later times
([56:10-14](https://quran.com/56/10-14){target="_quran"}) and the
verse that predicts that many Muslims will someday shun the Qur'an
([25:30](https://quran.com/25/30){target="_quran"}).

[^39]: Sahih al Bukhari: _The Early Years of Islam_, trans. and
explained by Muhammad Asad (Gibraltar: Dar at Andalus, 1981), 19.

Further insight into this question might be gained by examining more
closely what the Qur'an states necessitated Muhammad's prophethood.

The Qur'an presents itself and Muhammad's apostleship as the
culmination and completion of God's direct communication to mankind
through divinely inspired persons. Its many narratives about former
prophets confirm and reinforce Muhammad's mission. The Qur'an's
descriptions of the trials and obstacles faced by these previous
messengers very often parallel events in Muhammad's own struggle, and
the proclamations they make to their communities echo pronouncements
made elsewhere in the Qur'an.  This shows that the battle fought by
all prophets between good and evil, revelation and rejection, truth
and falsity, has always been the same.

The single most important fact governing all creation and preached by
all of God's messengers is that "there is no god but God" (in Arabic,
_la illaha illa Allah_). It implies that the many different objects of
worship chosen by men have no real authority or power and that the
divisions and hatreds to which such misdirected veneration lead are
totally unnecessary and a result of nothing more than evil and
self-destructive man-made illusions. It means there is but one
spiritual and moral standard governing humanity and but one measure of
a person's worth.

> O mankind! Lo! We have created you male and female and have made you
nations and tribes that you may know one another. Lo! the noblest
among you in the sight of God is the best in
conduct. ([49:13](https://quran.com/49/13){target="_quran"})

Most importantly, it implies that the barriers we set up between
ourselves and others are fallacies, because we all must answer to the
same supreme God.

In seventh-century Arabia, each tribe had its own deity from which it
sought protection and favoritism and to which it appealed in the
self-perpetuating intertribal strife. It took Islam's monotheism to
unite the warring factions as the Qur'an so poignantly reminds them.

> And hold fast, all of you together, to the rope of Allah, and do not
separate. And remember Allah's favor unto you: how you were enemies
and he made friendship between your hearts so that you became as
brothers by his grace; and (how) you were on the brink of an abyss of
fire: and he did save you from
it. ([3:103](https://quran.com/3/103){target="_quran"})

Islamic monotheism not only demands that we accept that there is only
one God, but also that we accept its natural corollary: All men and
women are in fact equal under God's authority. These two demands, the
oneness of God and the unity of humanity, have throughout history been
difficult to uphold in any religious tradition, as the cases of
Judaism and Christianity so powerfully demonstrate in the Qur'an.

The story of the Children of Israel is of a people who are uniquely
receptive throughout much of their history to monotheism despite their
existence within a predominantly pagan milieu. Outside influences
frequently penetrate their community and cause them to waver at times
from the teachings of their prophets. In the Qur'an, they appear as a
nation in constant struggle between pure monotheism and heathen
pressures, and this in part explains their need to insulate themselves
from their social surroundings and their attempt to preserve and
protect their racial and cultural purity. But they came to see
themselves as God's chosen people, to the exclusion of others, and as
sons of God in the Old Testament sense. As a result, they could never
accept the final messenger of God because of his non-Jewish origins,
even though he confirmed the essential message in their
scriptures. The Qur'an continuously blames them for their refusal in
this regard. In short, Judaism, although successful in preserving the
belief in one God, was unable to accept the oneness of man under God.

Christianity goes back to the same biblical roots. But much more than
Judaism, it is a universal religion. Its coherence derives from an
intense spiritual yearning to know and be loved by God. Thus, while
the Jews and the pagans of the Arabian peninsula were stubbornly
closed to a message that departed from their traditions, Christians
are shown to be more easily affected by its spiritual force
([4:85-89](https://quran.com/4/85-89){target="_quran"}).

The biggest difficulty encountered by such universal faiths is the
great diversity of peoples they absorb. Converts bring their own
languages, ideas, symbols, and cultural practices, all of which could
potentially distort the universal faith in question. From the Muslim
view, such was the case with Christianity: Although it eagerly
embraces all mankind, its tenets compromise pure monotheism and lend
too easily to associating others with God. In this way the
Judeo-Christian experience exemplifies the dilemma faced by all world
religions: Monotheism or universalism were invariably compromised in
attempting to preserve one or the other.

Islam also struggled -- and still struggles -- with these internal
tensions.  Eventually, extreme measures were taken by the mainstream
to protect both implications of monotheism. Philosophical and mystical
speculation were discouraged, all aspects of life were systematized
into religious law, and innovative thought was forbidden through the
adoption of _taqlid_ (unquestioning acceptance of earlier scholarly
opinion). Pressures continued to rise, but Islamic orthodoxy, for the
most part, succeeded in placing the major sources and ideas of early
mainstream Islam on ice, preserved in a type of suspended animation,
that eventually would be transferred to modern man intact. Whatever
the cost to Muslim civilization of the severe steps taken by these
Muslim scholars, the two major features of Islamic monotheism -- the
oneness of God and the oneness of humanity -- were united successfully
in Islam and passed on to future generations. For Muslims, this is one
example of how God, through Islam, completed His favor unto mankind
([5:3](https://quran.com/5/3){target="_quran"}).

This concern with preserving both aspects of monotheism helps to
explain the termination of prophethood with Muhammad. As long as a
religion anticipates a future revelation, the door is left open to
false prophets. Deceivers and self-deluded individuals invariably
emerge to mislead others and divide the community. A powerful source
of schism threatens the unity of the believers much more deeply and
permanently than any legal disputation. Every major religion,
including Islam, has known this danger, but the termination of
prophethood with Muhammad has greatly restrained this tendency. A
Muslim leader today may gain the admiration and allegiance of very
many followers, but it is nearly impossible to obtain their
unconditional trust -- that type of absolute loyalty obtained by a
perception of divine guidance. The moment a leader claims such a
status, his movement is invariably doomed to become a relatively
insignificant cult disconnected from the community of Muslims.

Recently, many Muslims were very curious about Rashad Khaleefah's
organization in Tucson, Arizona. When he declared himself to be
another messenger of God, however, virtually all Muslims dismissed and
ignored him, and he died with only a handful of disciples. Many
Western scholars refer to similar disenfranchised movements, such as
the Bahai's or Qadianis, as Islamic sects, although the designation is
inappropriate and misleading. The Muslim world does not consider these
groups to be alternative or even heretical perspectives within the
Islamic community; they are considered entirely outside of Islam. No
such movement has attracted a significant number of Muslims, although
they may win converts from other populations, because the belief in
Muhammad as the last and final prophet is one of Islam's principal
dogmas.

The _shahadah_ (testimony of faith) is the nearest thing to a creed in
Islam. It is recited at least nine times a day by observant Muslims in
their prayers. In the first half of the shahadah, a Muslim bears
witness that "there is no god but God" (_la ilaha illa Allah_), while
in the second half he or she testifies that "Muhammad is the messenger
of God" (_Muhammadan rasullu-Allah_). By the second statement, the
Muslim understands not only that Muhammad is God's messenger, but also
that he is the last prophet and the only one he should follow. Thus,
the _shahaddah_ connects Islamic monotheism to the finality of
Muhammad's mission. From the viewpoint of Muslims, his prophetic
vocation was necessitated by the need for a continual witness on earth
to both implications of monotheism -- the oneness of God and the
oneness of humanity under God -- and the sealing of prophethood with
him was necessary in order to preserve and safeguard that witness from
later fragmentation.
